import { currencyFormat } from "./currencyFormat";
export const calculateTotalPrice = (products) => {
    let totalPrice = 0;
    products.data.forEach((product) => {
        totalPrice += Number(product.price * product.qty_cart)
    })
    return currencyFormat(totalPrice);
}