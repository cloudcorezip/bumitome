export const currencyFormat = (num) => {
    var rupiah = '';
    var numrev = parseInt(num).toString().split('').reverse().join('');
    for (var i = 0; i < numrev.length; i++) if (i % 3 == 0) rupiah += numrev.substr(i, 3) + '.';
    return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}