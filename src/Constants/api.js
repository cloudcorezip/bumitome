export const base_url = "https://bumitome.com/wp-json/wc/v3"


// auth
export const sign_in_uri = "https://bumitome.com/wp-json/jwt-auth/v1/token/"

//PRODUCTS
export const get_product_categories_uri = base_url + "/products/categories";
export const get_products_uri = base_url + "/products";
export const wc_get_product_categories_uri = "products/categories";

//ORDER
export const order_uri = base_url + '/orders';

// CUSTOMER
// export const get_user_uri = base_url +  

export const get_cities_uri = 'https://bumitome.com/wp-content/plugins/plugin-ongkos-kirim/data/cities_search.json';
