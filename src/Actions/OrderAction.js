import * as ACTION_TYPE from "@constants";

export const addProductToCart = ( data ) => ({
    type: ACTION_TYPE.ADD_PRODUCT_TO_CART,
    payload: {
        data: data
    }
})

export const updateProductInCart = ( data ) => ({
    type: ACTION_TYPE.UPDATE_PRODUCT_IN_CART,
    payload: {
        data: data
    }
})