import * as ACTION_TYPE from "@constants";

export const storeUserProfile = (data) => ({
  type: ACTION_TYPE.STORE_USER_PROFILE,
  payload: {
    data: data
  }
})