import * as ACTION_TYPE from "@constants";

const initialState = {
  data: undefined,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPE.STORE_USER_PROFILE: 
      newState = { ...state };
      newState.data = action.payload.data
      return newState;
    default:
      return state;
  }
}
