import * as ACTION_TYPE from "@constants";

const initialState = {
    data: []
}

export default (state = initialState, action) => {
    switch(action.type){
        case ACTION_TYPE.ADD_PRODUCT_TO_CART:
            console.log("Reducer Add Product to Cart!")
            newState = { ...state }
            newState.data = state.data.concat(action.payload.data)
            return newState
        case ACTION_TYPE.UPDATE_PRODUCT_IN_CART:
            console.log("Reducer Update Product in Cart!")
            newState = { ...state }
            newState.data = action.payload.data
            return newState
        default: 
            return state
    }
}