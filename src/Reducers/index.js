import { combineReducers } from "redux";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LOGGED_OUT } from "@constants";

import CartReducer from "./CartReducer";
import UserReducer from './UserReducer';

const appReducer = combineReducers({
    productCart: CartReducer,
    userData: UserReducer
})

const rootReducer = (state, action) => {
    if(action.type === LOGGED_OUT) {
      AsyncStorage.removeItem('persist:root');
      console.log("logout redux action")
  
      state = undefined
    }
  
    return appReducer(state, action)
  }

export default rootReducer;