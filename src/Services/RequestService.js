import AsyncStorage from '@react-native-async-storage/async-storage';
import Axios from 'axios';
import WooCommerceAPI from 'react-native-woocommerce-api';

const axiosWithToken = Axios.create();

const WCApi = new WooCommerceAPI({
  url: 'https://bumitome.com', // Your store URL
  consumerKey: 'ck_ee6e6deed3617cce0e2e3469ba61040ce35a8552', // Your consumer secret
  consumerSecret: 'cs_a51efc9194925e4b2c4b41452f32b07189cff646', // Your consumer secret
  queryStringAuth: true,
  version: 'wc/v3'
})

axiosWithToken.interceptors.request.use(
  async config => {
    const token = await AsyncStorage.getItem('token')
    if (token) {
      config.headers = {
        'Authorization': `Bearer ${token}`
      }
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
);

// WooCommerce Services
export const postWithWCKey = (endpoint, data) => {
  return WCApi.post(endpoint, data);
}

export const getWithWCKey = (endpoint, param) => {
  return WCApi.get(endpoint, param)
}

//AXIOS Services
export const apiGetWithToken = (endpoint) => {
  return axiosWithToken.get(endpoint)
}

export const apiPostWithToken = (endpoint, data) => {
  return axiosWithToken.post(endpoint, data)
}

export const apiGet = (endpoint) => {
  return Axios.get(endpoint);
}

export const apiPost = (endpoint, body) => {
  return Axios.post(endpoint, body);
}