export const PRIMARY = '#198240';
export const SECONDARY = '#E5D1A6';
export const WHITE = '#FFFFFF';
export const BLACK = '#555555';
export const BLACK_LIGHT = '#A5B5B5';
export const ORANGE = '#FC8D3C';
export const DISABLE_PRIMARY = "rgba(25, 130, 64, 0.4)"

// ACTIONS
export const SUCCESS = '#3adb76';
export const WARNING = '#ffae00';
export const DANGER = '#cc4b37';

// GRAYSCALE
export const GRAY_LIGHT = '#F4F4F4';
export const GRAY_MEDIUM = '#EBEBEB';
export const GRAY_DARK = '#D7DBDB';