import React from "react";
import { Icon, Col } from "native-base";
import { View, Dimensions, StyleSheet } from "react-native";
import { Colors } from "@styles";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Home, OrderList, Category, History, Profile } from "@scenes";
const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
    tabCircular: {
        backgroundColor: Colors.PRIMARY,
        alignItems: "center",
        justifyContent: "center",
        width: width * 0.15, 
        height: width * 0.15, 
        borderRadius: 50, 
        marginBottom: 40, 
        borderWidth: 4, 
        borderColor: Colors.WHITE, 
        elevation: 5
    }
})

const Tab = createBottomTabNavigator()

const TabStack = () => {
    return (
        <Tab.Navigator
            tabBarOptions={{
                showLabel: false,
                activeTintColor: Colors.PRIMARY,
                inactiveTintColor: Colors.BLACK_LIGHT,
                keyboardHidesTabBar: true
            }}>

            <Tab.Screen
                name="home"
                component={Home}
                options={{
                    tabBarIcon: props => (
                        <Icon name="home" type="Feather" size={22} style={{ color: props.color }} />
                    )
                }}
            />
            <Tab.Screen
                name="orderList"
                component={OrderList}
                options={{
                    tabBarIcon: props => (
                        <Icon name="list" type="Feather" size={22} style={{ color: props.color }} />
                    )
                }}
            />
            <Tab.Screen
                name="category"
                component={Category}
                options={{
                    tabBarIcon: props => (
                        <View style={styles.tabCircular}>
                            <Icon name="shopping-bag" type="Feather" size={22} style={{ color: "#FFFFFF" }} />
                        </View>
                    )
                }}
            />
            <Tab.Screen
                name="history"
                component={History}
                options={{
                    tabBarIcon: props => (
                        <Icon name="clock" type="Feather" size={22} style={{ color: props.color }} />
                    )
                }}
            />
            <Tab.Screen
                name="profile"
                component={Profile}
                options={{
                    tabBarIcon: props => (
                        <Icon name="user" type="Feather" size={22} style={{ color: props.color }} />
                    )
                }}
            />

        </Tab.Navigator>
    )
}

export default TabStack;