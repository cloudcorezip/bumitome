import React from "react";
import { createStackNavigator, TransitionPresets } from "@react-navigation/stack";
import { SignIn, SignUp, ResetPassword, Register } from "@scenes";

const Stack = createStackNavigator();

const AuthStack = () => {
    return (
        <Stack.Navigator
            screenOptions={{
                gestureEnabled: true,
                gestureDirection: "horizontal",
                ...TransitionPresets.SlideFromRightIOS
            }}
        >
            <Stack.Screen
                name="signIn"
                component={SignIn}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="signUp"
                component={SignUp}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="resetPassword"
                component={ResetPassword}
                options={{
                    headerShown: false
                }}
            />
            {/* <Stack.Screen
                name="register"
                component={ResetPassword}
                options={{
                    headerShown: false
                }}
            /> */}
        </Stack.Navigator>
    )
}

export default AuthStack;