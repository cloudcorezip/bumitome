import React, { useState, useEffect } from "react";
import { createStackNavigator, TransitionPresets } from "@react-navigation/stack";
import { ListProduct, DetailProduct, OrderProduct, OrderDetail, Cart, OrderSuccess, OnBoarding, SearchProducts, SignUp } from "@scenes";
import AsyncStorage from '@react-native-async-storage/async-storage';
import TabStack from "../tab";

const Stack = createStackNavigator();

const useFirstInstall = () => {
    const [status, setStatus] = useState(true)
    const onFirstInstall = async () => {
        let firstInstall = await AsyncStorage.getItem("firstInstall")
        if (firstInstall !== null) {
            setStatus(false)
        }
    }
    useEffect(() => {
        onFirstInstall()
    }, [status])
    return status
}

const AppStack = () => {
    let onFirstInstall = useFirstInstall()
    return (
        <Stack.Navigator
            initialRouteName="home"
            screenOptions={{
                ...TransitionPresets.SlideFromRightIOS
            }}
        >

            {onFirstInstall ? (
                <Stack.Screen
                    name="onBoarding"
                    component={OnBoarding}
                    options={{
                        headerShown: false
                    }}
                />
            ) : (
                null
            )}
            <Stack.Screen
                name="home"
                component={TabStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="listProduct"
                component={ListProduct}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="detailProduct"
                component={DetailProduct}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="orderProduct"
                component={OrderProduct}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="orderDetail"
                component={OrderDetail}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="cart"
                component={Cart}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="orderSuccess"
                component={OrderSuccess}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="searchProducts"
                component={SearchProducts}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="signUp"
                component={SignUp}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
    )
}

export default AppStack;