import { getWithWCKey } from '@services';


export const getUserProfile = (id) => {
  return getWithWCKey('customers/' + id)
}