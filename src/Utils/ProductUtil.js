import { get_products_uri } from "@constants/api";
import { apiGet, apiGetWithToken, getWithWCKey } from '@services';

export const showProducts = (param = '' ) => {
    // return getWithWCKey("products" + "?stock_status=instock&category=" + categoryId + param);
    return getWithWCKey("products", param);
}

export const getDetailProduct = (productId) => {
    return getWithWCKey("products" + "/" + productId);
}

export const getProductCategories = () => {
return getWithWCKey("products" + "/" + "categories");
}

// export const getProductCategories = () => {
//     return getWithWCKey(get_products_uri + "/" + "categories");
// }

export const getProducts = (param = "") => {
    return getWithWCKey("products", param )
}