import { get_cities_uri } from '@constants/api';
import { apiGetWithToken } from '@services';

export const getCities = () => {
  return apiGetWithToken(get_cities_uri);
}