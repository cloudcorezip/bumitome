import { postWithWCKey, apiPost } from '@services';
import { sign_in_uri } from '@constants/api'


export const registerWc = (body) => {
  return postWithWCKey('customers', body);
}

export const signIn = (body) => {
  return apiPost(sign_in_uri, body)
}