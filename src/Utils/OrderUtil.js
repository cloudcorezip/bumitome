import { order_uri } from "@constants/api";
import { apiPostWithToken, apiGetWithToken } from "@services";

export const createOrder = (data) => {
  return apiPostWithToken(order_uri, data);
}

export const getOrders = () => {
  return apiGetWithToken(order_uri + "/32");
}

export const getOrderDetail = (orderId) => {
  return apiGetWithToken(order_uri + '/' + orderId)
}