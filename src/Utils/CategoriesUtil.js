import { wc_get_product_categories_uri } from "@constants/api"
import { getWithWCKey } from '@services';

export const showCategories = () => {
    return getWithWCKey("products/categories");
}