import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer, persistStore } from "redux-persist";
import { createStore } from "redux";
import { name as appName } from "../../app.json";
import rootReducer from "@reducers";

const persistConfig = {
    key: 'root',
    blacklist: [],
    whitelist: ['productCart', 'userData'],
    keyPrefix: appName,
    storage: AsyncStorage
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default () => {
    let store = createStore(persistedReducer)
    let persistor = persistStore(store)
    return { store, persistor }
}