import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
import { color } from "react-native-reanimated";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        marginTop: width * 0.01,
        flexDirection: "row",
        alignItems: "center",
    },
    star: {
        width: width * 0.03,
        height: width * 0.03
    },
    text: {
        fontSize: Normalize(11),
        color: Colors.BLACK_LIGHT,
        marginLeft: 10,
        marginRight: 10
    }
})