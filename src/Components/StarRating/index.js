import React from "react";
import { View, Text, Image } from "react-native";
import Starfilled from "@assets/star-filled.png"
import StarUnfilled from "@assets/star-unfilled.png";
import styles from "./styles";

const StarRating = (props) => {
    let stars = [];

    for(var i = 1; i <= 5; i++){
        let path = Starfilled;
        if(i > props.ratings){
            path = StarUnfilled;

        }
        stars.push((<Image source={path} key={i} style={styles.star}/>))
    }
    
    return (
        <View style={styles.container}>
            {stars}
            <Text style={styles.text}>({props.views})</Text>
        </View>
    )
}

export default StarRating;