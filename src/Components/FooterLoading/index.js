import React from 'react';
import { ActivityIndicator, View , Dimensions, StyleSheet } from 'react-native';
import { Colors } from "@styles";

const FooterLoading = (props) => {
  if (!props.isLoading) return null;
  return (
    <View
      style={styles.loadingContainer}>
      <ActivityIndicator style={{ marginLeft: 8 }} animating size="large" />
    </View> 
      // <ActivityIndicator color={Colors.PRIMARY} animating size="large" /> 
  );
};

const styles = StyleSheet.create({
  loadingContainer: {
    position: 'relative',
    paddingVertical: 5,
    borderColor: Colors.PRIMARY
  }
})

export default FooterLoading;