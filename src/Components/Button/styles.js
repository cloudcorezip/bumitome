import { StyleSheet } from "react-native";
import { Colors, Normalize } from "@styles";

export default StyleSheet.create({
    container: {
        backgroundColor: Colors.PRIMARY,
        height: 50,
        borderRadius: 75,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center"
    },
    disabledContainer: {
        backgroundColor: Colors.DISABLE_PRIMARY,
        height: 50,
        borderRadius: 75,
        alignItems: "center",
        justifyContent: "center",
        alignSelf: "center"
    },  
    title: {
        fontSize: Normalize(14),
        color: Colors.WHITE
    }
})