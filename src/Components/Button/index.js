import React from "react";
import { TouchableOpacity, Text, Dimensions } from "react-native";
const { width } = Dimensions.get("window");
import styles from "./styles";

const Button = (props) => (
    <TouchableOpacity onPress={props.onPress} disabled={props.disabled} style={[props.disabled ? styles.disabledContainer : styles.container, {width: props.width != undefined ? width * props.width : null}]}>
        <Text style={styles.title}>{props.title}</Text>
    </TouchableOpacity>
)
export default Button;