import { StyleSheet } from "react-native";
import { Colors, Normalize } from "@styles";

export default StyleSheet.create({
    container: {
        backgroundColor: Colors.WHITE
    },
    title: {
        color: Colors.BLACK
    }
})