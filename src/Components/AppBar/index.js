import React from "react";
import { Header, Left, Body, Right, Title, Icon } from "native-base";
import styles from "./styles";

const AppBar = (props) => (
    <Header
        {...props} 
        style={{
            backgroundColor: props.backgroundColor == undefined ? (
                "#198240"
            ) : (
                props.backgroundColor
            )
        }} 
        iosBarStyle={props.barStyle == undefined ? (
                "light-content"
            ) : (
                props.barStyle
            ) 
        } 
        androidStatusBarColor={props.statusBarColor == undefined ? (
                "#127236"
            ) : (
                props.statusBarColor
            )
        } 
        noShadow>
        
        {props.children}
    </Header>
)

export default AppBar;