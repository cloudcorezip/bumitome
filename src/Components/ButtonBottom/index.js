import React from "react";
import {
    Text,
    ActivityIndicator,
    View, TouchableWithoutFeedback
} from 'react-native';
import { Font, StC, Colors } from "@styles";
import LinearGradient from "react-native-linear-gradient";

const ButtonBottom = (props) => (
    <TouchableWithoutFeedback onPress={props.disabled ? null : props.handleOnPress}>
        <View style={{flex:1}}>
            {props.loading ? (
                <ActivityIndicator size="large" color="#FFFFFF" />
            ):(
                <Text style={[Font.interSemiBold12px, StC.colorWhite]}>{props.title}</Text>
            )}
        </View>
    </TouchableWithoutFeedback>
)

export default ButtonBottom
