import React from "react";
import { View, Text, TouchableWithoutFeedback } from "react-native";
import { Thumbnail } from "native-base";
import styles from "./styles";
import { currencyFormat } from "@constants/currencyFormat";

const ProductCard = (props) => (
    <TouchableWithoutFeedback onPress={props.onPress}>
        <View style={styles.cardContainer}>
            { props.thumbnail != undefined &&
                <View style={styles.cardLeft}>
                    <Thumbnail square large source={props.thumbnail[0] ? {uri: props.thumbnail[0].src} : require('@assets/icon/agriculture3x.png')}  />
                </View>
            }
            <View style={styles.cardMiddle}>
                <Text style={styles.productTitle}>{props.name}</Text>
                <Text style={styles.productQty}>Jumlah: {props.qty === null ? 0 : props.qty}</Text>
            </View>
            <View style={styles.cardRight}>
                <Text style={styles.productPrice}>{props.price != '' ? currencyFormat(props.price) : 'Rp. 0'}</Text>
            </View>
        </View>
    </TouchableWithoutFeedback>
)

export default ProductCard;