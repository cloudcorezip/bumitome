import React from "react";
import {
    View,
    Text,
    Image,
    Modal,
    StatusBar,
} from 'react-native';
import { ButtonBottom,  } from "@components";
// import * as Animatable from 'react-native-animatable';
import {RFValue} from 'react-native-responsive-fontsize';

const ModalSuccess = (props) => (
    <Modal animationType="fade" transparent={true} visible={props.visible}>
        <StatusBar
            backgroundColor={'#127236'}
            barStyle={'light-content'}
        />
        <View style={styles.centered}>
            <Animatable.View animation={"bounceInUp"} iterationDelay={50} style={styles.modalContainer}>
                <View style={{justifyContent: 'center', alignItems: 'center', marginBottom: RFValue(10)}}>
                    <Image
                        style={styles.image}
                        source={props.icon}
                    />
                </View>
                <View style={styles.contentMessage}>
                    {props.title && (
                        <Text>{props.title}</Text>
                    )}
                    <Text >{props.message}</Text>
                </View>
                <View style={styles.contentFooter}>
                    <ButtonBottom
                        title={props.btn}
                        handleOnPress={props.onPressOutside}
                    />
                </View>
            </Animatable.View>
        </View>
    </Modal>
)

export default ModalSuccess;

const styles = ({
    modalContainer: {
        borderRadius:RFValue(15),
        width:RFValue(230),
        paddingHorizontal: RFValue(5),
        paddingTop: RFValue(30),
        paddingBottom: RFValue(10),
        backgroundColor: '#FFFF'
    },
    image:{
        width: RFValue(120),
        height: RFValue(120),
        resizeMode:'contain',
    },
    centered:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
    },
    contentMessage:{
        marginHorizontal:RFValue(10),
        alignItems:'center',
        marginBottom:RFValue(20),
        paddingTop: RFValue(4)
    },
    contentFooter:{
        height:RFValue(35),
        marginHorizontal: RFValue(30),
        marginBottom: RFValue(15)
    }
})