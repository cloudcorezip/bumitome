import React, { useEffect, useState } from 'react';
import { FlatList, Modal, Text, TouchableOpacity, View } from 'react-native';
import { Body, Button, Icon, Left, Right, Title } from 'native-base';
import SearchInput, { createFilter } from 'react-native-search-filter';
import { AppBar } from '@components';
import { getCities } from "@utils";


const KEYS_TO_FILTERS = ['name']

const ItemSeparator = () => (
  <View style={{ marginVertical: 15,  borderBottomWidth: 0.5 }} />
)


const GetLocationNamesModal = (props) => {
  const [citiesData, setCitiesData] = useState(undefined)
  const [searchTerm, setSearchTerm] = useState('')

  getAllCities = () => {
    getCities().then(res => {
      let arr = []
      Object.keys(res.data).forEach((key) => {
        let val = res.data[key];
        let temp = {
          'id': key,
          'name': val
        }
        arr.push(temp);
      })
      setCitiesData(arr)
    }).catch(err => {
      console.log(err)
    })
  }

  const searchUpdated = (text) => {
    setSearchTerm(text)
  }

  useEffect(() => {
    getAllCities();
  }, [])

  
  // if(citiesData !== undefined ) {
    const filteredCitiesData = citiesData == undefined ? [] : citiesData.filter(createFilter(searchTerm, KEYS_TO_FILTERS))  
  // }

  return (
    <Modal
      visible={props.isVisible}
      // animationType="slide"
      onRequestClose={props.onClose}
    >
      <View style={{ flex: 1 }}>
        <AppBar>
          <Left style={{ flex: 0.15 }}>
            <Button transparent onPress={props.onClose}>
              <Icon name="arrow-left" type="Feather" />
            </Button>
          </Left>
          <Body>
            <Title>Cari Kota</Title>
          </Body>
          <Right style={{ flex: 0.15 }} />
        </AppBar>
        <SearchInput 
          onChangeText={(term) => { searchUpdated(term) }}
          placeholder="Cari Kota ... "
          style={{ padding: 10, borderColor: '#CCC', borderWidth: 0.5 }}
        />
        <FlatList 
          data={filteredCitiesData}
          keyExtractor={(item, index) => index.toString()}
          contentContainerStyle={{ padding: 15 }}
          ItemSeparatorComponent={(props) => {
            return ( <ItemSeparator />)
          }}
          renderItem={({ item, index}) => (
            <TouchableOpacity onPress={() => props.onCitySelect(item)}> 
              <Text>{item.name}</Text>
            </TouchableOpacity>
          )}
        />
      </View>
    </Modal>

  )
}


export default GetLocationNamesModal;