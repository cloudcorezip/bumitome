import React from "react";
import { View, ActivityIndicator, Modal } from "react-native";
import { Container } from "native-base";
import styles from "./styles";


const LoadingActivity = (props) => {
    return (
        <Modal
            transparent={true}
            visible={props.isLoading}>
            <View style={{ flex: 1, backgroundColor: "rgba(0, 0, 0, 0.2)", alignItems: "center", justifyContent: "center" }}>
                <View style={styles.spinnerContainer}>
                    <ActivityIndicator size="large" color="#198240" />
                </View>
            </View>
        </Modal>
    )
}

export default LoadingActivity;