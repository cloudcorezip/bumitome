import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(0,0,0, 0.1)"
    },
    spinnerIcon: {
        color: Colors.WHITE,
        fontSize: Normalize(35)
    },
    spinnerContainer: {
        width: width * 0.18,
        height: width * 0.18,
        borderRadius: 5,
        backgroundColor: "rgba(0,0,0, 0.05)",
        alignItems: "center",
        justifyContent: "center"
    }
})