import React, { useState, useContext } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Container, Form, Item, Input, Label } from "native-base";
import { Button } from "@components";
import { AuthContext } from "@constants/context";
import styles from "./styles";

const InputItem = (props) => (
    <Item stackedLabel style={styles.item}>
        <Label style={styles.itemLabel}>{props.label}</Label>
        <Input {...props} />
    </Item>
)

const SignIn = ({ navigation }) => {
    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')

    const { signIn } = useContext(AuthContext)

    const isSignIn = () => {
        console.log(email, password)
        // signIn(email, password)
    }

    return (
        <Container>
            <View style={styles.headerContainer}>
                <Image source={require('@assets/logo.png')} style={styles.headerLogo} />
            </View>
            <View style={styles.bodyContainer}>
                <View style={styles.itemContainer}>
                    <Form>
                        <InputItem 
                            label="Email"
                            onChangeText={(email) => setEmail(email)}
                            value={email}
                        />
                        <InputItem 
                            label="Password" 
                            secureTextEntry={true}
                            onChangeText={(password) => setPassword(password)}
                            value={password}
                        />
                    </Form>
                </View>
                <Button title="Masuk" width={0.85} onPress={isSignIn}/>
                <TouchableOpacity style={styles.forgotPassword} onPress={() => navigation.navigate('resetPassword')}>
                    <Text style={styles.forgotPasswordText}>Lupa Password?</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.footerContainer}>
                <Text style={styles.footerText}>Belum punya akun?</Text>
                <TouchableOpacity style={styles.footerButton} onPress={() => navigation.navigate('signUp')}>
                    <Text style={styles.footerTextButton}> Daftar disini</Text>
                </TouchableOpacity>
            </View>
        </Container>
    )
}

export default SignIn;