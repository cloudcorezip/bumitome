import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    headerContainer: {
        flex: 1.2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerLogo: {
        width: width * 0.45,
        height: width * 0.20,
        marginTop: width * 0.05
    },

    bodyContainer: {
        flex: 1.5
    },
    itemContainer: {
        marginLeft: width * 0.07,
        marginRight: width * 0.07,
        marginBottom: width * 0.15
    },
    item: {
        borderBottomWidth: 2,
        borderBottomColor: Colors.PRIMARY,
        marginLeft: 0,
        marginBottom: width * 0.05
    },
    itemLabel: {
        color: Colors.PRIMARY
    },

    forgotPassword: {
        alignItems: "center",
        justifyContent: "center",
        marginTop: width * 0.05
    },
    forgotPasswordText: {
        color: Colors.PRIMARY,
        fontSize: Normalize(12),
        fontWeight: "bold"
    },

    footerContainer: {
        flex: 0.3,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "row"
    },
    footerButton: {
        alignItems: "center",
        justifyContent: "center"
    },
    footerText: {
        color: Colors.PRIMARY,
        fontSize: Normalize(12),
        fontWeight: "bold"
    },
    footerTextButton: {
        color: Colors.PRIMARY,
        fontSize: Normalize(12),
        fontWeight: "bold"
    }
})