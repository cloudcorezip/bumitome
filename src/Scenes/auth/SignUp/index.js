import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Container, Left, Body, Icon, Title, Form, Item, Input, Label } from "native-base";
import { AppBar, Button } from "@components";
import { registerWc } from "@utils";
import styles from "./styles";
import { RFValue } from "react-native-responsive-fontsize";

const InputItem = (props) => (
    <View>
        <Item stackedLabel style={styles.itemContainer}>
            <Label style={styles.itemLabel}>{props.label}</Label>
            <Input {...props} style={{ padding: 2 }}/>
        </Item>
        <Text style={props.isNotValid ? { color: 'red'} : { color: 'white'} }>{props.errorText}</Text>
    </View>
)

const SignUp = ({ navigation }) => {
    const [state, setState] = useState({
        password: ''
    })


    const updateForm = (stateName, value) => {
        setState(prevState => ({
            ...prevState,
            [stateName]: value
        }))
    }

    const handleSubmit = () => {
        registerWc({
            email: state.email,
            password: state.password
        }).then(res => {
            if(res.data !== undefined) {
                alert(res.message)
            } else {
                navigation.navigate('profile');
                alert('Daftar Akun Sukses')
            }
        }).catch(err => {
            console.log(err)
        })
    }

    const validate = (stateName, text) => {
        if(stateName == 'email') {
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/
            return reg.test(text);
        } else {
            // console.log('n')
            return text.length > 4;
        }
    }

    let isFormValidate = !state.email || !state.password || !validate('email', state.email) || !validate('password', state.password)

    return (
        <Container>
            <AppBar
                backgroundColor="#FFF"
                statusBarColor="#FFF"
                barStyle="dark-content"
            >
                <Left style={{ flex: 0.15 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name="arrow-left" type="Feather" />
                    </TouchableOpacity>
                </Left>
                <Body>
                    <Title style={styles.appBartitle}>Register</Title>
                </Body>
            </AppBar>
            <View style={styles.bodyContainer}>
                <Form style={styles.formContainer}>
                    <InputItem 
                        label="Email" 
                        onChangeText={(text) => updateForm('email', text)}
                        errorText="Email kurang tepat"
                        isNotValid={!validate('email', state.email) && !state.email == ''}
                    />
                    <View style={{ marginBottom: RFValue(20)}}/>
                    <InputItem 
                        label="Passsword" 
                        onChangeText={(text) => updateForm('password', text)} 
                        isNotValid={!validate('password', state.password) && !state.password == '' }
                        errorText='Password harus lebih dari 4 karakter'
                    />
                </Form>
                <Button disabled={isFormValidate} title="Register" width={0.85} onPress={() => handleSubmit()}/>
            </View>
        </Container>
    )
}

export default SignUp;