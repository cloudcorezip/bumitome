import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
import { RFValue } from "react-native-responsive-fontsize";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    appBartitle: {
        color: Colors.BLACK
    },
    headerContainer: {
        flex: 0.3, 
        justifyContent: "center"
    },
    headerTitle: {
        fontSize: Normalize(14),
        color: Colors.BLACK,
        marginLeft: width * 0.08,
        marginRight: width * 0.08
    },
    bodyContainer: {
        marginTop: RFValue(30),
        flex: 1
    },
    formContainer: {
        marginLeft: width * 0.08,
        marginRight: width * 0.08,
        marginTop: width * 0.05,
        marginBottom: width * 0.1
    },
    itemContainer: {
        borderBottomWidth: 2,
        borderBottomColor: Colors.PRIMARY,
        marginLeft: 0,
        // marginBottom: width * 0.05
    },
    itemLabel: {
        // marginBottom: 15,
        color: Colors.PRIMARY
    },
    footerContainer: {
        flex: 0.3
    },
    buttonContainer: {
        flexDirection: 'row', 
        alignItems: "flex-end", 
        justifyContent: "center",
        marginTop: width * 0.2
    },
    footerText: {
        color: Colors.PRIMARY,
        fontSize: Normalize(12),
        fontWeight: "bold"
    },
    footerTextButton: {
        color: Colors.PRIMARY,
        fontSize: Normalize(12),
        fontWeight: "bold"
    }
})