import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { Container, Left, Body, Title, Icon, Form, Item, Input, Label } from "native-base";
import { AppBar, Button } from "@components";
import styles from "./styles";

const ResetPassword = ({ navigation }) => {
    return (
        <Container>
            <AppBar
                backgroundColor="#FFF"
                statusBarColor="#FFF"
                barStyle="dark-content"
            >
                <Left style={{ flex: 0.15 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon name="arrow-left" type="Feather" />
                    </TouchableOpacity>
                </Left>
                <Body>
                    <Title style={styles.appBartitle}>Reset Password</Title>
                </Body>
            </AppBar>
            <View style={styles.headerContainer}>
                <View style={styles.headerLableContainer}>
                    <Text style={styles.headerLable}>Konfirmasi email anda dan kami akan mengirim petunjuk ganti password</Text>
                </View>
            </View>
            <View style={styles.bodyContainer}>
                <Form style={styles.form}>
                    <Item stackedLabel style={styles.item}>
                        <Label style={styles.label}>Alamat Email</Label>
                        <Input/>
                    </Item>
                </Form>
                <Button title="Reset Password" width={0.7} />
            </View>
        </Container>
    )
}

export default ResetPassword;