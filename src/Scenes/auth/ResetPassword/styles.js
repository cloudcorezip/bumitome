import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window")

export default StyleSheet.create({
    container: {

    },
    appBartitle: {
        color: Colors.BLACK
    },
    headerContainer: {
        flex: 1, 
        justifyContent: "center", 
        alignItems: "center"
    },
    headerLableContainer: {
        marginLeft: width * 0.08, 
        marginRight: width * 0.08,
    },
    headerLable: {
        fontSize: Normalize(16)
    },

    bodyContainer: {
        flex: 3
    },
    form: {
        marginLeft: width * 0.08,
        marginRight: width * 0.08
    },
    item: {
        borderBottomWidth: 2,
        borderBottomColor: Colors.PRIMARY,
        marginLeft: 0,
        marginBottom: width * 0.1
    },
    label: {
        color: Colors.PRIMARY
    }
})