import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        backgroundColor: Colors.GRAY_LIGHT
    },
    formContainer: {
        backgroundColor: Colors.WHITE,
        marginBottom: width * 0.025,
    },
    segmentContainer: {
        margin: width * 0.03,
    },
    orderListContainer: {
      flex: 1,
      backgroundColor: Colors.WHITE,
    },
    labelSegment: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK,
        marginBottom: width * 0.05
    },
    labelDeleteActive: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.PRIMARY,
        marginBottom: width * 0.05
    },
    labelDeleteInActive: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK_LIGHT,
        marginBottom: width * 0.05
    },
    item: {
        marginLeft: 0,
    },
    iconInputAddress: {
        fontSize: Normalize(16),
        color: Colors.BLACK
    },

    textArea: {
        width: '100%',
        marginTop: 10,
        borderRadius: 5, 
        color: Colors.BLACK
    },

    // footer
    footerContainer: {
        height: width * 0.15,
        backgroundColor: Colors.WHITE,
        flexDirection: "row",
        elevation: 20,
    },
    footerLeft: {
        flex: 0.6,
        alignItems: "center",
        justifyContent: "center",
    },
    footerRight: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    labelPrice: {
        fontSize: Normalize(14)
    },
    valuePrice: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.PRIMARY,
    },
    button: {
        width: width * 0.6,
        backgroundColor: Colors.ORANGE,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        height: width * 0.1,
        borderRadius: 5,
        
    },
    buttonIcon: {
        color: Colors.WHITE,
        marginRight: width * 0.02,
        fontSize: Normalize(16)
    },
    buttonTitle: {
        color: Colors.WHITE,
        fontSize: Normalize(14),
        fontWeight: "bold"
    },

    // modal
    modalContainer: {
        flex: 1,
        alignItems: "center", 
        justifyContent: "center"
    },
    popupContainer: {
        backgroundColor: Colors.WHITE,
        height: width * 0.3,
        width: width * 0.9,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10
    },
    popupContainerDeleteProduct: {
        backgroundColor: Colors.WHITE,
        height: width * 0.4,
        width: width * 0.9,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 10
    },
    popupTitle: {
        fontSize: Normalize(16),
        fontWeight: "bold",
        color: Colors.BLACK
    },
    popupDescription: {
        fontSize: Normalize(14),
        color: Colors.BLACK,
        textAlign: "center"
    },
    buttonContainer: {
        flexDirection: "row",
        marginLeft: width * 0.05,
        marginRight: width * 0.05,
        marginTop: width * 0.06
    },
    buttonCancel: {
        flex: 1,
        height: width * 0.08,
        // borderWidth: 1,
        // borderColor: Colors.BLACK,
        backgroundColor: Colors.GRAY_MEDIUM,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5,
        marginRight: "2%"
    },
    buttonCancelText: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK
    },
    buttonConfirmed: {
        flex: 1,
        height: width * 0.08,
        backgroundColor: Colors.PRIMARY,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5,
        marginLeft: "2%"
    },
    buttonConfirmedText: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.WHITE
    },


    cardContainer: {
        flex: 1,
        backgroundColor: "#FFF",
        borderRadius: 5,
        width: width * 0.8,
        flexDirection: "row",
        marginBottom: width * 0.03,
        padding: width * 0.02,
        zIndex: 2,
        elevation: 4
    },
    cardLeft: {
        justifyContent: "center",
        alignItems: "flex-start",
        marginRight: width * 0.03
    },
    cardMiddle: {
        justifyContent: "center",
    },
    productTitle: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK,
    },
    productQty: {
        fontSize: Normalize(14),
        color: Colors.BLACK
    },
    productPrice: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.PRIMARY
    }
})