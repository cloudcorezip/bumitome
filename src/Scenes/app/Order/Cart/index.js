import React, { useState, useEffect, useCallback } from "react";
import { View, TouchableOpacity, TouchableWithoutFeedback, Dimensions, TextInput, RefreshControl, ActivityIndicator } from "react-native";
import { Container, Text, Left, Body, Right, Button, Icon, Title, Form, Item, Label, Input, Row, Content, Thumbnail, Textarea } from "native-base";
import CheckBox from '@react-native-community/checkbox';
import Modal from 'react-native-modal';
import { useSelector, useDispatch } from "react-redux";
import { AppBar, GetLocationNamesModal } from "@components";
import { Colors } from "@styles";
import { currencyFormat, calculateTotalPrice } from "@constants";
import { updateProductInCart } from "@actions";
import { createOrder } from "@utils";

const { width, height } = Dimensions.get("window")
import styles from "./styles";

// const LocationInputItem = (props) => {

// }

const ProductItem = (props) => {
    return (
        <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 0.15, justifyContent: "center" }}>
                    <CheckBox
                        disabled={false}
                        value={props.checked}
                        boxType={'square'}
                        tintColors={{ true: '#198240', false: '#A5B5B5' }}
                        onValueChange={props.onValueChange}
                    />
                </View>
                <TouchableWithoutFeedback onPress={props.onPress}>
                    <View style={styles.cardContainer}>
                        <View style={styles.cardLeft}>
                            <Thumbnail square large source={props.thumbnail ? {uri: props.thumbnail[0].src} : require('@assets/icon/agriculture3x.png') } />
                        </View>
                        <View style={styles.cardMiddle}>
                            <Text style={styles.productTitle}>{props.name}</Text>
                            <Text style={styles.productQty}>Berat: {props.weight * props.qty} Kg</Text>
                            <Text style={styles.productQty}>Jumlah: {props.qty}</Text>
                            <Text style={styles.productPrice}>{props.price}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
            </View>
    )
}

const Cart = ({ navigation }) => {
    const [addressModal, setAddressModal] = useState(false)
    const [selectedCity, setSelectedCity] = useState("");
    const [tempCity, setTempCity] = useState([])
    const [isOrderConfirmed, setOrderConfirmed] = useState(false);
    const [isDeleteConfirmed, setDeleteConfirmed] = useState(false);
    const [productSelected, setProductSelected] = useState([]);
    const [totalPrice, setTotalPrice] = useState(0);
    const [desc, setDesc] = useState('');
    const [data, setData] = useState({ country: 'IDN' });
    const [miscData, setMiscData] = useState({});
    const [isRefresh, setIsRefresh] = useState(false);
    const productInCart = useSelector(state => state.productCart);
    const dispatch = useDispatch();

    useEffect(() => {
        getTotal()
    }, [])


    const handleCheckBox = (productId) => {
        var selected = {...productSelected};
        if(selected[productId]){
            selected[productId] = false
            const id = productId.toString()
            const newSelected = Object.keys(selected).reduce((object, key) => {
                if(key !== id){
                    object[key] = selected[key]
                }
                return object
            }, {})
            selected = newSelected
        }else{
            selected[productId] = true;
        }
        setProductSelected(selected)
    }

    const getTotal = () => {
        setTotalPrice(calculateTotalPrice(productInCart))
    }

    const isFormEmpty = () => {

        return !(
            miscData.email &&
            miscData.phone &&
            data.first_name &&
            data.last_name &&
            data.address_1 &&
            // data.province &&
            // data.city && 
            data.postcode
        )
    }

    const handleFormEmpty = () => {
        if(isFormEmpty()) {
            alert('Silahkan Lengkapi data anda');
        } else {
            handleCreateOrder();
        }
    }

    const onSelectedCity = (val) => {
        setTempCity([])
        setSelectedCity(val.name)
        let arr = [];
        productInCart.data.forEach(cart => {
            let city = cart.meta_data[cart.meta_data.length - 1].value
            if(city != val.name) {
                let temp = {
                    'name': cart.name,
                    'city': cart.meta_data[cart.meta_data.length - 1].value
                }
                arr.push(temp)
            }
        })
        setTempCity(arr);
        setAddressModal(false)
    }
    
    const handleCreateOrder = () => {
        setIsRefresh(true)
        let tempCart = productInCart.data.map((obj) => {
            return {
                product_id: obj.id,
                quantity: obj.qty_cart
            }
        });
        let tempData = {...data, ...miscData}
        createOrder({
            "payment_method": "bacs",
            "payment_method_title": "Direct Bank Transfer",
            "set_paid": true,
            "billing": tempData,
            "shipping": data,
            'line_items': tempCart,
            "shipping_lines": [
                {
                  "method_id": "flat_rate",
                  "method_title": "Flat Rate",
                  "total": "10.00"
                }
              ]
        }).then((res) => {
            if(res.status === 201) {
                dispatch(updateProductInCart([]));
                setIsRefresh(false);
                setOrderConfirmed(false);
                navigation.navigate('orderSuccess');
            } else {
                alert("Data yang di kirimkan salah mohon dicek kembali");
                setIsRefresh(false);
                setOrderConfirmed(false)
            }
        }).catch((err) => {
            alert(err.message)
        })
    }


    const removeProductFromCart = useCallback(() => {
        let product = productInCart.data
        let productKey = Object.keys(productSelected)
        let results = []
        productKey.forEach((key) => {
            var removeIndex = product.map((item) => {return item.id}).indexOf(parseInt(key))
            product.splice(removeIndex, 1)
            results = product
        })
        dispatch(updateProductInCart(results))
        if(selectedCity != "") {
            onSelectedCity({ name: selectedCity })
        }
        setDeleteConfirmed(false)
        setProductSelected([])
    }, [productSelected])

    return (
        <Container style={styles.container}>
            <AppBar>
                <Left style={{ flex: 0.15 }}>
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name="arrow-left" type="Feather" />
                    </Button>
                </Left>
                <Body>
                    <Title>Keranjang</Title>
                </Body>
                <Right style={{ flex: 0.15 }} />
            </AppBar>
            <Content showsVerticalScrollIndicator={false}>
                <View style={styles.formContainer}>
                    <View style={styles.segmentContainer}>
                        <Text style={styles.labelSegment}>Lengkapi Data Diri</Text>
                        <Form style={styles.form}>
                            <Item stackedLabel style={styles.item}>
                                <Label>First name</Label>
                                <Input onChangeText={(text) => setData({ ...data, first_name: text})} />
                            </Item>
                            <Item stackedLabel style={styles.item}>
                                <Label>Last name</Label>
                                <Input onChangeText={(text) => setData({...data, last_name: text})}/>
                            </Item>
                            <Item stackedLabel style={styles.item}>
                                <Label>Email</Label>
                                <Input onChangeText={(text) => setMiscData({ ...miscData, email: text})}/>
                            </Item>
                            <Item stackedLabel style={styles.item}>
                                <Label>Nomor Telepon</Label>
                                <Input keyboardType='number-pad' onChangeText={(text) => setMiscData({ ...miscData, phone: text})}/>
                            </Item>
                        </Form>
                    </View>
                </View>
                <View style={styles.formContainer}>
                    <View style={styles.segmentContainer}>
                        <Text style={styles.labelSegment}>Lengkapi Data Alamat</Text>
                        <Form style={styles.form}>
                            <Item stackedLabel style={styles.item}  onPress={() => setAddressModal(true)}>
                                <Label>Kota</Label>
                                {/* <Input onChangeText={(text) => setData({ ...data, address_1: text})}/> */}
                                <Input disabled value={selectedCity} />
                            </Item>
                            {tempCity.map((item, key) => (
                                <Text style={{ fontSize: 12, marginLeft: 5, marginVertical: 5, color: '#FB0404' }}>Barang {item.name} hanya tersedia di {item.city}</Text>
                            ))

                            }
                            <Item stackedLabel style={styles.item}>
                                <Label>Alamat </Label>
                                <Input onChangeText={(text) => setData({ ...data, address_1: text})}/>
                            </Item>
                            <Item stackedLabel style={styles.item}>
                                <Label>Kode Pos</Label>
                                <Input onChangeText={(text) => setData({ ...data,postcode: text}) }/>
                            </Item>
                            <Item stackedLabel style={styles.item}>
                                <Label>Keterangan Lainnya</Label>
                                    <Textarea onChangeText={(text) => setDesc(text)} value={desc} rowSpan={4} bordered placeholder="Opsional" placeholderTextColor="rgba(85,85,85, 0.32)" style={styles.textArea} />
                            </Item>
                        </Form>
                    </View>
                </View>
                <View style={styles.orderListContainer}>
                    <View style={styles.segmentContainer}>
                        <View style={{ flexDirection: "row" }}>
                            <View style={{ flex: 1 }}>
                                <Text style={styles.labelSegment}>Daftar Pesanan</Text>
                            </View>
                            <View style={{ flex: 1, alignItems: "flex-end" }}>
                                <TouchableOpacity onPress={() => setDeleteConfirmed(true)} disabled={Object.keys(productSelected).length === 0 ? true : false}>
                                    <Text style={Object.keys(productSelected).length === 0 ? styles.labelDeleteInActive : styles.labelDeleteActive}>Hapus</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {productInCart.data.length > 0 ? (
                            productInCart.data.map((item, key) => (
                                <ProductItem
                                    key={key}
                                    name={item.name}
                                    item={item}
                                    thumbnail={item.images}
                                    qty={item.qty_cart}
                                    weight={item.weight}
                                    price={currencyFormat(item.price * item.qty_cart)}
                                    checked={productSelected[item.id]}
                                    onValueChange={() => handleCheckBox(item.id)}
                                    onPress={() => navigation.navigate('orderDetail', { data: item, cartIndex: key, origin: 'cart' })}
                                />
                            ))
                        ) : (
                            <Text>*Tidak ada pesanan dalam keranjang</Text>
                        )}
                    </View>
                </View>
            </Content>
            {productInCart.data.length > 0 ? (
                <View style={styles.footerContainer}>
                    <View style={styles.footerLeft}>
                        <Text style={styles.labelPrice}>Total Harga</Text>
                        {  totalPrice !== 0 &&
                            <Text style={styles.valuePrice}>{totalPrice}</Text>
                        }
                    </View>
                    <View style={styles.footerRight}>
                        <TouchableOpacity style={[styles.button, selectedCity == "" || tempCity.length > 0 ? { backgroundColor: '#cccccc'} : null]} disabled={selectedCity == "" ? true : false} onPress={() => setOrderConfirmed(true)}>
                            <Icon name="check-circle" type="Feather" style={styles.buttonIcon} />
                            <Text style={styles.buttonTitle}>Beli</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            ) : (
                    null
            )}

            {/* Confirm order popup */}
            <Modal
                deviceWidth={width}
                deviceHeight={height}
                animationIn="slideInDown"
                isVisible={isOrderConfirmed}>
                <View style={styles.modalContainer}>
                    <View style={styles.popupContainer}>
                        <Text style={styles.popupTitle}>Konfirmasi Pesanan?</Text>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.buttonCancel} onPress={() => setOrderConfirmed(false)}>
                                <Text style={styles.buttonCancelText}>Batal</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonConfirmed} onPress={() => isRefresh === true  ? null : handleFormEmpty() }>
                                { isRefresh === false &&
                                    <Text style={styles.buttonConfirmedText}>Konfirmasi</Text>
                                }
                                { isRefresh &&
                                    <ActivityIndicator color={Colors.WHITE} />
                                }
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>

            {/* Delete Popup */}
            <Modal
                deviceWidth={width}
                deviceHeight={height}
                animationIn="slideInUp"
                isVisible={isDeleteConfirmed}>
                <View style={styles.modalContainer}>
                    <View style={styles.popupContainerDeleteProduct}>
                        <Text style={styles.popupTitle}>Hapus {Object.keys(productSelected).length} Barang?</Text>
                        <Text style={styles.popupDescription}>Barang yang kamu pilih akan dihapus dari keranjangmu</Text>
                        <View style={styles.buttonContainer}>
                            <TouchableOpacity style={styles.buttonCancel} onPress={() => setDeleteConfirmed(false)}>
                                <Text style={styles.buttonCancelText}>Kembali</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.buttonConfirmed} onPress={removeProductFromCart}>
                                <Text style={styles.buttonConfirmedText}>Hapus Barang</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </Modal>
            <GetLocationNamesModal 
                isVisible={addressModal}
                onClose={() => setAddressModal(false)}
                onCitySelect={(item) => onSelectedCity(item)}
            />                      
        </Container>
    )
}

export default Cart;