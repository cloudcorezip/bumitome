import React, { useState, useEffect } from "react";
import { View, TouchableOpacity, Image, FlatList, RefreshControl } from "react-native";
import { Container, Body, Title, Text, Content, Left, Right, Button, Icon, Badge } from "native-base";
import { AppBar } from "@components";
import { useSelector } from "react-redux";
import { showCategories } from "@utils";
import styles from "./styles";

const useCategories = () => {
    const [categories, setCategories] = useState([])
    const [isLoading, setLoading] = useState(true)
    const requestCategories = () => {
        showCategories().then(res => {
            // if (res.request.status === 200) {
                setLoading(false)
                setCategories(res)
            // } else {
            //     console.log("[REQUEST] Categories code => " + res.request.status)
            // }
        }).catch(res => {
            setLoading(false)
            console.log(res)
        })
    }

    const handleRefresh = () => {
        setLoading(true)
        setTimeout(() => {
            requestCategories()
        }, 1000)
    }

    useEffect(() => {
        requestCategories()
    }, [isLoading])

    return { categories, isLoading, handleRefresh }
}

const Category = ({ navigation }) => {
    const productsInStorage = useSelector(state => state.productCart)
    const { categories, isLoading, handleRefresh } = useCategories()

    return (
        <Container>
            <AppBar>
                <Left style={{ flex: 0.5 }} />
                <Body style={{ alignItems: "center" }}>
                    <Title>Pesan</Title>
                </Body>
                <Right style={{ flex: 0.5 }}>
                    {productsInStorage.data.length > 0 ? (
                        <Button transparent style={{ justifyContent: "flex-end" }} onPress={() => navigation.navigate('cart')}>
                            <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                            <Badge style={{ scaleX: 0.8, scaleY: 0.8, position: 'absolute' }} danger>
                                <Text>{productsInStorage.data.length}</Text>
                            </Badge>
                        </Button>
                    ) : (
                            <Button transparent onPress={() => navigation.navigate('cart')}>
                                <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                            </Button>
                        )}
                </Right>
            </AppBar>
            <View style={styles.container}>
                <FlatList
                    data={categories}
                    numColumns={2}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) => (
                        <View style={{ flex: 1, flexDirection: 'column', margin: 7 }}>
                            <TouchableOpacity style={styles.card} onPress={() => navigation.navigate('orderProduct', {categoryId: item.id, categoryName: item.name})}>
                                <Image source={item.image === null ? require('@assets/icon/plant3x.png') : { uri: item.image.src }} style={styles.categoryIcon} />
                                <Text style={styles.categoryTitle}>{item.name}</Text>
                            </TouchableOpacity>
                        </View>
                    )}
                    refreshControl={
                        <RefreshControl
                            refreshing={isLoading}
                            onRefresh={handleRefresh}
                        />
                    }
                />
            </View>
        </Container>
    )
}

export default Category;