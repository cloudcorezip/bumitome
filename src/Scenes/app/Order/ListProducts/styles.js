import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
import { Col } from "native-base";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    searchContainer: {
        backgroundColor: '#EBEBEB',
        padding: width * 0.024,
    },
    searchItem: {
        backgroundColor: "#D7DBDB",
        borderRadius: 75,
        height: 40  
    },
    searchInput: {
        paddingLeft: width * 0.03
    },
    flatListContainer: {
        marginTop: width * 0.02
    },

    footerContainer: {
        height: width * 0.15,
        backgroundColor: Colors.WHITE,
        flexDirection: "row",
        elevation: 20,
    },
    footerLeft: {
        flex: 0.6,
        alignItems: "center",
        justifyContent: "center",
    },
    footerRight: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    labelPrice: {
        fontSize: Normalize(14)
    },
    valuePrice: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.PRIMARY,
    },
    button: {
        width: width * 0.6,
        backgroundColor: Colors.ORANGE,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        height: width * 0.1,
        borderRadius: 5,
        
    },
    buttonIcon: {
        color: Colors.WHITE,
        marginRight: width * 0.02
    },
    buttonTitle: {
        color: Colors.WHITE,
        fontSize: Normalize(14),
        fontWeight: "bold"
    }
})