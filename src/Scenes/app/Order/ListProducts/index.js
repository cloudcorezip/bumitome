import React, { useState, useEffect } from "react";
import { View, FlatList, TouchableOpacity, RefreshControl, Animated, Dimensions } from "react-native";
import { Container, Left, Body, Right, Title, Icon, Button, Item, Input, Badge, Text, Subtitle } from "native-base";
import { AppBar, FooterLoading } from "@components";
import styles from "./styles";
import { ProductCard } from "@components";
import { useSelector } from "react-redux";
import { calculateTotalPrice } from "@constants";
import { showProducts } from "@utils"

const { height } = Dimensions.get('window')

const useProducts = (categoryId) => {
    let scrollViewHeight = 0;
    const [products, setProducts] = useState([])
    const [isLoading, setLoading] = useState(true)
    const [footerLoading, setFooterLoading] = useState(false)
    const [offsetParam, setOffsetParam] = useState(0)
    const [maxData, setMaxData] = useState(false)

    const requestProducts = (offset = 0) => {
        showProducts({ category: categoryId, stock_status: 'instock', offset: offset }).then(res => {
            if(res.length > 0) {
                if(offset == 0) {
                    setProducts(res)
                } else [
                    setProducts(products.concat(res))
                ]
            }
            if(res.length == 0) {
                setMaxData(true)
            }
            setFooterLoading(false)
            // if(res.request.status === 200){
            //     if(res.data.length > 0) {
            //         setProducts( products.concat(res.data))
            //     }
            //     if(res.data.length == 0 ) {
            //         setMaxData(true)
            //     }
            //     setFooterLoading(false)
            // }else{
            //     console.log("[REQUEST] Products code => " + res.request.status)
            // }
            setLoading(false)
        }).catch(err => {
            console.log(err)
        })
    }
    const handleRefresh = () => {
        setLoading(true)
        setMaxData(false)
        requestProducts()
    }
     
    const handleLoadMore = (e) => {
        if(!maxData) {
            setFooterLoading(true)
            setOffsetParam( offsetParam + 10 )
        }
    }

    useEffect(() => {
        requestProducts(offsetParam)
    }, [isLoading, offsetParam])
    return { products, isLoading, handleRefresh, handleLoadMore, footerLoading}
}

const ListProduct = ({ route, navigation }) => {
    const productsInStorage = useSelector(state => state.productCart)
    const [ offsetParam, setOffsetParam ] = useState(0)
    const [y , setY ] = useState(new Animated.Value(0))
    const { categoryId, categoryName } = route.params
    const {products, isLoading, handleRefresh, handleLoadMore, footerLoading } = useProducts(categoryId)

    return (
        <Container>
            <AppBar>
                <Left style={{ flex: 0.5 }}>
                    <Button transparent onPress={() => navigation.navigate('home')}>
                        <Icon name="arrow-left" type="Feather" />
                    </Button>
                </Left>
                <Body style={{ alignItems: "center" }}>
                    <Title>Daftar Produk</Title>
                    <Subtitle>{categoryName}</Subtitle>
                </Body>
                <Right style={{ flex: 0.5 }}>
                    {productsInStorage.data.length > 0 ? (
                            <Button transparent style={{ justifyContent: "flex-end" }} onPress={() => navigation.navigate('cart')}>
                                <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                                <Badge style={{ scaleX: 0.8, scaleY: 0.8, position: 'absolute' }} danger>
                                    <Text>{productsInStorage.data.length}</Text>
                                </Badge>
                            </Button>
                    ) : (
                            <Button transparent onPress={() => navigation.navigate('cart')}>
                                <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                            </Button>
                    )}

                </Right>
            </AppBar>
            <View style={styles.searchContainer}>
                <Item regular style={styles.searchItem}>
                    <Input
                        style={styles.searchInput}
                        placeholder="Search . . ."
                        placeholderTextColor="rgba(160, 160, 160, 60)"
                    />
                </Item>
            </View>
            <FlatList
                showsVerticalScrollIndicator={false}
                style={styles.flatListContainer}
                data={products}
                keyExtractor={(item, index) => index.toString()}
                // onScroll={Animated.event(
                //     [{ nativeEvent: { contentOffset: { y: y}}}],
                //     {
                //         listener: event => {
                //             handleLoadMore(event)
                //         }
                //     }
                // )}
                renderItem={({ item, index }) => (
                    <ProductCard
                        name={item.name}
                        item={item}
                        thumbnail={item.images}
                        qty={item.stock_quantity === null ? 0 : item.stock_quantity}
                        price={item.price}
                        onPress={() => navigation.navigate('orderDetail', { data: item, origin: 'listProduct' })}
                    />
                )}
                refreshControl={
                    <RefreshControl
                      refreshing={isLoading}
                      onRefresh={handleRefresh}
                    />
                }
                onEndReachedThreshold={0.01}
                onEndReached={info => {
                    handleLoadMore(info)
                }}
                ListFooterComponent={<FooterLoading isLoading={footerLoading} />}
            />
            {productsInStorage.data.length > 0 ? (
                    <View style={styles.footerContainer}>
                        <View style={styles.footerLeft}>
                            <Text style={styles.labelPrice}>Total Harga</Text>
                            <Text style={styles.valuePrice}>{calculateTotalPrice(productsInStorage)}</Text>
                        </View>
                        <View style={styles.footerRight}>
                            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('cart', {products: productsInStorage})}>
                                <Icon name="shopping-cart" type="Feather" style={styles.buttonIcon} />
                                <Text style={styles.buttonTitle}>Keranjang</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                ) : (
                    null
                )}
        </Container>
    )
}

export default ListProduct;