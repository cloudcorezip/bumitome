import React, { useEffect } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Container, Icon, Right } from "native-base";
import { AppBar } from "@components";
import styles from "./styles";

const OrderSuccess = ({ navigation }) => {
    useEffect(() => {
        const timeout = setTimeout(() => {
            navigation.navigate('home')
        }, 8000)
        return () => clearTimeout(timeout)
    }, [])
    return (
        <Container style={styles.container}>
            <AppBar
                backgroundColor="#FFF"
                statusBarColor="#FFF"
                barStyle="dark-content"
            >
                <Right>
                    <TouchableOpacity style={{marginRight: "2%"}} onPress={() => navigation.navigate('home')}>
                        <Icon name="close" type="AntDesign" />
                    </TouchableOpacity>
                </Right>
            </AppBar>
            <View style={styles.bodyContainer}>
                <Image source={require('@assets/animation_success.gif')} style={styles.gifAnimation}/>
            </View>
            <View style={styles.footerContainer}>
                <View style={styles.footerTop}>
                    <Text style={styles.footerTopText}>Pesanan anda telah diterima</Text>
                </View>
                <View style={styles.footerBottom}> 
                    <Image resizeMode="cover" source={require('@assets/curved.png')} style={styles.footerCurved}/>
                </View>
            </View>
        </Container>
    )
}

export default OrderSuccess;