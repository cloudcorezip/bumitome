import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.WHITE
    },
    bodyContainer: {
        flex: 1.5,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Colors.WHITE
    },
    footerContainer: {
        flex: 1,
    },
    gifAnimation: {
        width: width * 0.9,
        height: width * 0.9
    },

    footerTop: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    footerTopText: {
        fontSize: Normalize(18),
        fontWeight: "bold",
        color: Colors.BLACK
    },
    footerBottom: {
        flex: 1,
        justifyContent: "flex-end"
    },
    footerCurved: {
        width: width,
        height: width * 0.21
    }
})