import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        backgroundColor: "#F7F7F7"
    },
    productContainer: {
        flex: 1,
    },
    cardProduct: {
        flexDirection: "row",
        padding: width * 0.04,
        backgroundColor: Colors.WHITE
    },
    productThumbnail: {
        flex: 0.45,
    },
    thumbnail: {
        width: width * 0.25,
        height: width * 0.25
    },
    productInfo: {
        flex: 1,
        justifyContent: "center"
    },
    productName: {
        fontSize: Normalize(16),
        fontWeight: "bold",
        color: Colors.BLACK
    },
    productPrice: {
        fontSize: Normalize(15),
        fontWeight: "bold",
        color: Colors.PRIMARY
    },

    productInformation: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        marginTop: width * 0.025,
        paddingTop: width * 0.02,
        paddingLeft: width * 0.04,
        paddingRight: width * 0.04
    },
    informationLabel: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK,
    },
    
    productAdditionalInformation: {
        flex: 0.5,
        backgroundColor: Colors.WHITE,
        marginTop: width * 0.025,
        padding: width * 0.04,
        height: width * 0.4
    },

    qtyContainer: {
        flexDirection: "row", 
        alignItems: "center", 
        marginTop: width * 0.022
    },
    btnQty: {
        width: width * 0.06, 
        height: width * 0.06, 
        backgroundColor: "#D6D5D5", 
        alignItems: "center", 
        justifyContent: "center", 
        borderRadius: 75
    },
    btnQtyIcon: {
        fontSize: Normalize(18), 
        color: Colors.WHITE
    },
    itemQty: {
        width: width * 0.15, 
        height: width * 0.084
    },
    inputQty: {
        textAlign:"center"
    },
    form: {
        marginTop: width * 0.04
    },
    textArea: {
        borderRadius: 5, 
        color: Colors.BLACK
    },

    footerContainer: {
        height: width * 0.15,
        backgroundColor: Colors.WHITE,
        flexDirection: "row",
        elevation: 20,
    },
    footerLeft: {
        flex: 0.6,
        alignItems: "center",
        justifyContent: "center",
    },
    footerRight: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    labelPrice: {
        fontSize: Normalize(14)
    },
    valuePrice: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.PRIMARY,
    },
    button: {
        width: width * 0.6,
        backgroundColor: Colors.ORANGE,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        height: width * 0.1,
        borderRadius: 5,
        
    },
    buttonIcon: {
        color: Colors.WHITE,
        marginRight: width * 0.01,
        fontSize: Normalize(16)
    },
    buttonTitle: {
        color: Colors.WHITE,
        fontSize: Normalize(14),
        fontWeight: "bold"
    }
})