import React, { useState, useEffect, useCallback, useRef } from "react";
import { View, Text, Dimensions, TouchableOpacity, BackHandler, TouchableWithoutFeedback } from "react-native";
import { Container, Left, Body, Right, Button, Icon, Title, Content, Thumbnail, Item, Input, Form, Textarea } from "native-base";
import { AppBar, LoadingActivity } from "@components";
import { currencyFormat } from "@constants";
import HTML from 'react-native-render-html';
import { useSelector, useDispatch } from "react-redux";
import { addProductToCart, updateProductInCart } from "@actions";
import FlashMessage, { showMessage, hideMessage } from "react-native-flash-message";
import styles from "./styles";
import Modal from 'react-native-modal';
import ImageViewer from 'react-native-image-zoom-viewer';

const useImagePreview = (items) => {
    const [images, setImages] = useState([])
    const [onFetch, setOnFetch] = useState(true)
    useEffect(() => {
        const setImagePreview = () => {
            let results = []
            items.images.forEach((data) => {
                let val = {
                    url: data.src,
                    props: {}
                }
                results.push(val)
            })
            setImages(results)
            setOnFetch(false)
        }
        setImagePreview()
    }, [onFetch])
    return images
}

const OrderDetail = ({ route, navigation }) => {
    const stock = route.params.data.stock_quantity
    const [qty, setQty] = useState(0);
    const [isLoading, setLoading] = useState(false);
    const [imagePreview, setImagePreview] = useState(false)
    const { data, origin, cartIndex } = route.params;
    const productsImages = useImagePreview(data)

    const products = useSelector(state => state.productCart)
    const dispatch = useDispatch();

    useEffect(() => {
        handleQty();
        const backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            handleGoBack
        )
        return () => backHandler.remove();
    }, [])

    const handleQty = () => {
        if(origin == 'cart') {
            setQty(data.qty_cart)
        }
    }

    const updateCartQty = () => {
        setLoading(true)
        let productsArr = products.data
        if(qty > 0) {
            productsArr[cartIndex].qty_cart = qty
            dispatch(updateProductInCart(productsArr))
            setTimeout(() => {
                setLoading(false)
                showMessage({
                    message: "Selamat!",
                    description: "Product berhasil ditambahkan dalam keranjang belanja!",
                    type: "success"
                });
            }, 1000)
        } else {
            productsArr.splice(cartIndex, 1)
            dispatch(updateProductInCart(productsArr))
            setTimeout(() => {
                setLoading(false)
                showMessage({
                    message: "Selamat!",
                    description: "Product berhasil dihapus dalam keranjang belanja!",
                    type: "success"
                });
            }, 1000)
            handleGoBack()
        }
    }

    const addToCart = (productId) => {
        setLoading(true) // show loading activity
        setTimeout(() => setLoading(false), 1000)
        const findout = products.data.some(element => element.id === productId);
        if(!findout){
            data["qty_cart"] = qty;
            dispatch(addProductToCart(data));
            setTimeout(() => {
                showMessage({
                    message: "Selamat!",
                    description: "Product berhasil ditambahkan dalam keranjang belanja!",
                    type: "success"
                });
            }, 1000)
        }else{
            setTimeout(() => {
                showMessage({
                    message: "Perhatian!",
                    description: "Product ini sudah tersedia dalam keranjang anda! Mohon lanjutkan pembelian",
                    type: "warning"
                });
            }, 1000)
        }
    }

    const setQuantity = () => {}

    const handleGoBack = () => {
        hideMessage();
        navigation.goBack(null)
        return true
    }
    return (
        <Container style={styles.container}>
            <AppBar>
                <Left style={{ flex: 0.15 }}>
                    <Button transparent onPress={() => handleGoBack()}>
                        <Icon name="arrow-left" type="Feather" />
                    </Button>
                </Left>
                <Body style={{}}>
                    <Title>Detail Pesanan</Title>
                </Body>
                <Right style={{ flex: 0.15 }} />
            </AppBar>
            <Content>
                <View style={styles.productContainer}>
                    <View style={styles.cardProduct}>
                        <View style={styles.productThumbnail}>
                            <TouchableWithoutFeedback onPress={() => setImagePreview(true)}>
                                <Thumbnail square large source={data.images[0] ? {uri: data.images[0].src} : require('@assets/icon/agriculture3x.png') } style={styles.thumbnail} />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={styles.productInfo}>
                            <Text style={styles.productName}>{data.name}</Text>
                            <Text style={styles.productPrice}>{currencyFormat(data.price)}</Text>
                            <View style={styles.qtyContainer}>
                                <TouchableOpacity style={styles.btnQty} onPress={() => qty === 0 ? null : setQty(qty - 1)}>
                                    <Icon name="minus" type="Feather" style={styles.btnQtyIcon} />
                                </TouchableOpacity>
                                <Item style={styles.itemQty}>
                                    <Input keyboardType="number-pad" value={qty === null ? '0' : qty.toString()} onChangeText={(text) => setQty(text)} maxLength={2} style={styles.inputQty} disabled={qty === null ? true : false}/>
                                </Item>
                                <TouchableOpacity style={styles.btnQty} onPress={() => qty === stock ? null : setQty(qty + 1)}>
                                    <Icon name="plus" type="Feather" style={styles.btnQtyIcon} />
                                </TouchableOpacity>
                            </View>
                            <Text>Stock : {stock}</Text>
                        </View>
                    </View>
                    <View style={styles.productInformation}>
                        <Text style={styles.informationLabel}>Informasi Produk</Text>
                        <HTML html={data.description} baseFontStyle={{ fontSize: 16 }} imagesMaxWidth={Dimensions.get('window').width} />
                        <HTML html={data.short_description} baseFontStyle={{ fontSize: 14 }} imagesMaxWidth={Dimensions.get('window').width } />
                    </View>
                    { data.meta_data[data.meta_data.length - 1].value != ""  &&
                        <View style={styles.productInformation}>
                            <Text style={styles.informationLabel}>Lokasi Produk</Text>
                            <Text>Hanya Tersedia di {data.meta_data[data.meta_data.length - 1].value}</Text>
                        </View>
                    }
                    <View style={styles.productAdditionalInformation}>
                        <Text style={styles.informationLabel}>Keterangan</Text>
                        <Form style={styles.form}>
                            <Textarea rowSpan={4} bordered placeholder="Catatan toko" placeholderTextColor="rgba(85,85,85, 0.32)" style={styles.textArea} />
                        </Form>
                    </View>
                </View>
            </Content>
            <View style={styles.footerContainer}>
                <View style={styles.footerLeft}>
                    <Text style={styles.labelPrice}>Total Harga</Text>
                    <Text style={styles.valuePrice}>{currencyFormat(data.price*qty)}</Text>
                </View>
                <View style={styles.footerRight}>
                    {origin == 'cart' ? (
                        <TouchableOpacity style={styles.button} onPress={() => updateCartQty()}>
                            <Icon name="save" type="Feather" style={styles.buttonIcon} />
                            <Text style={styles.buttonTitle}>Simpan</Text>
                        </TouchableOpacity>
                    ) : (
                        <TouchableOpacity style={styles.button} onPress={() => addToCart(data.id)}>
                            <Icon name="plus" type="Feather" style={styles.buttonIcon} />
                            <Text style={styles.buttonTitle}>Keranjang</Text>
                        </TouchableOpacity>
                    )}

                </View>
            </View>
            <FlashMessage
                position="top"
                position={{ top: Dimensions.get('window').width * 0.13 }}
            />
            <LoadingActivity isLoading={isLoading} />
            <Modal 
                isVisible={imagePreview} 
                backdropOpacity={0.8}
                animationIn="zoomIn"
                animationOut="slideOutDown"
                useNativeDriver={true}>
                <View style={{ flex: 1 }}>
                    <ImageViewer 
                        imageUrls={productsImages}
                        backgroundColor="transparent"
                        enableSwipeDown={imagePreview}
                        onSwipeDown={() => setImagePreview(false)}
                        useNativeDriver={true}
                        onSave={() => alert("Fitur masih dalam develop")}
                        onClick={() => setImagePreview(false)}/>
                </View>
            </Modal>
        </Container>
    )
}

export default OrderDetail;