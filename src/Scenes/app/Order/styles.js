import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        paddingBottom: width * 0.05,
        marginTop: width * 0.02
    },
    cardContainer: {
        marginBottom: width * 0.03,
        flexDirection: "row"
    },
    card: {
        flex: 1, 
        // width: width / 2.1,
        backgroundColor: Colors.WHITE,
        elevation: 5,
        borderRadius: 10,
        height: width * 0.45, 
        // margin: 5,
        alignItems: "center",
        justifyContent: "center",
        padding: width * 0.02,
    },

    categoryIcon: {
        width: width * 0.2,
        height: width * 0.2
    },
    categoryTitle: {
        fontSize: Normalize(14),
        color: Colors.BLACK,
        marginTop: width * 0.03,
        textAlign: "center"
    }
    
})