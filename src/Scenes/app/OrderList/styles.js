import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    searchContainer: {
        backgroundColor: '#EBEBEB',
        padding: width * 0.024,
    },
    searchItem: {
        backgroundColor: "#D7DBDB",
        borderRadius: 75,
        height: 40  
    },
    searchInput: {
        paddingLeft: width * 0.03
    },
    bodyContainer: {
        flex: 1
    },
    card: {
        marginBottom: width * 0.03
    },
    cardContainer: {
        backgroundColor: '#FFFFFF',
        marginLeft: width * 0.03,
        marginRight: width * 0.03,
        marginTop: width * 0.03,
        borderRadius: 5,
        elevation: 5
    },
    cardHeader: {
        borderBottomWidth: 0.5,
        borderBottomColor: "rgba(112, 112, 112, 0.4)",
        flexDirection: "row",
        padding: width * 0.04
    },
    cardHeaderLeft: {
        flex: 1,
        flexDirection: "column"
    },
    cardHeaderTitle: {
        fontSize: Normalize(14),
        color: Colors.BLACK_LIGHT
    },
    cardHeaderSubtitle: {
        fontSize: Normalize(16),
        color: Colors.BLACK,
        fontWeight: "bold"
    },
    cardHeaderRight: {
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-end"
    },
    cardHeaderStatus: {
        color: Colors.PRIMARY,
        fontSize: Normalize(14)
    },
    cardBody: {
        flexDirection: "column",
        padding: 15
    },
    cardBodyLabel: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK_LIGHT
    },
    cardBodyValue: {
        fontSize: Normalize(16),
        fontWeight: "bold",
        color: Colors.ORANGE
    }
})