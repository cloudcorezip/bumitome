import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        paddingBottom: width * 0.05,
        // height: height
    },
    backBox: {
        height: width * 0.1,
        width: width,
        backgroundColor: Colors.PRIMARY,
        position: "absolute",
        top: 0,
        left: 0,
        zIndex: 1
    },
    contentContainer: {
        // paddingBottom: width * 0.1,
        // marginBottom: width * 0.1,
    },
    cardContainer: {
        backgroundColor: "#FFF",
        borderRadius: 5,
        width: width * 0.95,
        alignSelf: "center",
        flexDirection: "row",
        marginBottom: width * 0.03,
        padding: width * 0.02,
        zIndex: 2,
        elevation: 4
    },
    cardLeft: {
        flex: 0.5,
        justifyContent: "center",
        alignItems: "center"
    },
    cardMiddle: {
        flex: 1,
        justifyContent: "center"
    },
    cardRight: {
        flex: 0.5,
        justifyContent: "center",
        alignItems: "center"
    },
    productTitle: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK,
    },
    productQty: {
        fontSize: Normalize(14),
        color: Colors.BLACK
    },
    productPrice: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.PRIMARY
    }
})