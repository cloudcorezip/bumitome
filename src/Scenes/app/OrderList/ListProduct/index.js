import React, { useEffect, useState } from "react";
import { View, FlatList, ActivityIndicator} from "react-native";
import { Container, Body, Title, Left, Right, Button, Icon } from "native-base";
import styles from "./styles";
import { Colors } from '@styles'
import { getOrderDetail } from '@utils';
import { AppBar, ProductCard } from "@components";
import { currencyFormat } from "@constants";

import { products } from "./products";

const ListProduct = ({ route, navigation }) => {
    const [data, setData] = useState({
        detail: [],
        loading: true
    })

    const onRefresh = React.useCallback(() => {
        _getOrderDetail();
    })

    const _getOrderDetail = () => {
        getOrderDetail(route.params.data.orderId)
            .then(res => {
                console.log(res.data)
                setData({
                    detail: res.data,
                    loading: false
                })
            }).catch(err => [
                alert(err)
            ])
    }

    useEffect(() => {
        onRefresh();
    }, [])

    return (
        <Container>
            <AppBar>
                <Left style={{ flex: 0.5 }}>
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name="arrow-left" type="Feather" />
                    </Button>
                </Left>
                <Body style={{ alignItems: "center" }}>
                <Title>#{data.detail.number}</Title>
                </Body>
                <Right style={{ flex: 0.5 }} />
            </AppBar>
            <View style={styles.container}>
                <View style={styles.backBox}></View>
                <View style={{ zIndex: 3 }}>
                    <View style={styles.contentContainer}>
                        <FlatList
                            showsVerticalScrollIndicator={false}
                            data={data.detail.line_items}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <>
                                    <ProductCard
                                        name={item.name}
                                        qty={item.quantity}
                                        price={item.price}
                                        thumbnail={undefined}
                                        onPress={() => navigation.navigate('detailProduct', {data: { productId: item.product_id, qty: item.quantity}})}
                                    />
                                </>
                            )}
                        />
                    </View>
                </View>
            </View>           
        </Container>
    )
}

export default ListProduct;