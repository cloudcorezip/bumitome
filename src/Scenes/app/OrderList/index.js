import React, { useEffect, useState } from "react";
import { View, Text, TouchableWithoutFeedback, FlatList, RefreshControl } from "react-native";
import { Container, Left, Body, Title, Icon, Content, Item, Input } from "native-base";
import { currencyFormat } from '@constants';
import { getOrders } from "@utils";
import { Col, Row, Grid } from 'react-native-easy-grid';
import { AppBar } from "@components";
import styles from "./styles";
import { RFValue } from "react-native-responsive-fontsize";
import moment from 'moment';
import { useFocusEffect } from "@react-navigation/native";

const OrderList = ({ navigation }) => {
    const [searchText, setSearchText] = useState('');
    const [data, setData] = useState({
        orders: [],
        loading: true
    });

    const onRefresh = React.useCallback(() => {
        _getOrders();
    })

    const _getOrders = () => {
        getOrders()
            .then(res => {
                console.log(res.data)
                setData({
                    orders: res.data.filter(x => x.status !== 'completed'),
                    loading: false
                })
            })
            .catch(err => {
                if(JSON.parse(err.request._response).data.status == 401) {
                    alert('Anda belum Login')
                    setData({
                        orders: [],
                        loading: false
                    })
                }
                // alert('Terjadi Kesalahan', err.message)
            })
    }

    useFocusEffect(
        React.useCallback(() => {
            const unsub = navigation.addListener('blur', () => {
                setData({
                    orders: [],
                    loading: true
                })
            });
            
            _getOrders();

            return unsub;

        }, [navigation])
    )

    return (
        <Container>
            <AppBar>
                <Body style={{ alignItems: "center" }}>
                    <Title>Daftar Pesanan</Title>
                </Body>
            </AppBar>
            <View style={styles.bodyContainer}>
                <FlatList
                    data={data.orders}
                    contentContainerStyle={{ paddingBottom: RFValue(10) }}
                    keyExtractor={(index, item) => index.toString()}
                    refreshControl={
                        <RefreshControl
                            refreshing={data.loading}
                            onRefresh={onRefresh}
                        />
                    }
                    renderItem={({ item, index }) => (
                        <TouchableWithoutFeedback onPress={() => navigation.navigate('listProduct', { data: { orderId: item.id } })}>
                            <Grid style={styles.cardContainer}>
                                <Row style={styles.cardHeader}>
                                    <View style={styles.cardLeft}>
                                        <Text style={styles.cardHeaderTitle}>{moment(item.date_created).format('DD MMMM YYYY')}</Text>
                                        <Text style={styles.cardHeaderSubtitle}># {item.number}</Text>
                                    </View>
                                    <View style={styles.cardHeaderRight}>
                                        <Text style={styles.cardHeaderStatus}>{item.status}</Text>
                                    </View>
                                </Row>
                                <Row style={styles.cardBody}>
                                    <Text style={styles.cardBodyLabel}>Total Harga</Text>
                                    <Text style={styles.cardBodyValue}>{currencyFormat(item.total)}</Text>
                                </Row>
                            </Grid>
                        </TouchableWithoutFeedback>
                    )}
                />
            </View>
        </Container>
    )
}

export default OrderList;