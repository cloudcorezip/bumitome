import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        backgroundColor: "#F7F7F7"
    },
    productContainer: {
        flex: 1,
    },
    cardProduct: {
        flexDirection: "row",
        padding: width * 0.04,
        backgroundColor: Colors.WHITE
    },
    productThumbnail: {
        flex: 0.45,
    },
    thumbnail: {
        width: width * 0.25,
        height: width * 0.25
    },
    productInfo: {
        flex: 1,
        justifyContent: "center"
    },
    productName: {
        fontSize: Normalize(16),
        fontWeight: "bold",
        color: Colors.BLACK
    },
    productPrice: {
        fontSize: Normalize(15),
        fontWeight: "bold",
        color: Colors.PRIMARY
    },
    productQty: {
        fontSize: Normalize(14),
        color: Colors.BLACK
    },

    productInformation: {
        flex: 1,
        backgroundColor: Colors.WHITE,
        marginTop: width * 0.025,
        paddingTop: width * 0.02,
        paddingLeft: width * 0.04,
    },
    informationLabel: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK,
    },
    informationContent: {
        fontSize: Normalize(14),
        color: Colors.BLACK,
        marginTop: width * 0.02
    },
    
    productAdditionalInformation: {
        flex: 0.5,
        backgroundColor: Colors.WHITE,
        marginTop: width * 0.025,
        padding: width * 0.04,
        height: width * 0.4
    }
})