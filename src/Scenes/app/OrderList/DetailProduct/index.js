import React, { useEffect, useState } from "react";
import { View, Text, Dimensions, ActivityIndicator } from "react-native";
import { Container, Left, Body, Right, Button, Icon, Title, Content, Thumbnail } from "native-base";
import { AppBar } from "@components";
import { Colors } from "@styles";
import { getDetailProduct } from '@utils';
import { currencyFormat } from "@constants";
import HTML from 'react-native-render-html';
import styles from "./styles";
import { RFValue } from "react-native-responsive-fontsize";

const htmlContent = `
        <p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s</p>
        <p>Manfaat :&nbsp;</p>
        <ol>
            <li>Detoksifikasi tubuh</li>
            <li>Bagus untuk kesehatan mata</li>
            <li>Bagus untuk pencernaan</li>
            <li>Meningkatkan sistem kekebalan tubuh</li>
            <li>Menjaga kesehatan tulang</li>
            <li>Bagus untuk ibu hamil</li>
        </ol>
    `

const DetailProduct = ({ route, navigation }) => {
    const [data, setData] = useState({
        detail:  undefined,
        loading: true
    })

    const _getDetailProduct = () => {
        getDetailProduct(route.params.data.productId)
            .then(res => {
                console.log(res)
                setData({ 
                    detail: res,
                    loading: false
                })
            }).catch(err => {
                console.log(err)
                alert('Terjadi Kesalahan', err)
            })
    }

    useEffect(() => {
        _getDetailProduct();
    }, [])


    return (
        <Container style={styles.container}>
            <AppBar>
                <Left style={{ flex: 0.15 }}>
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name="arrow-left" type="Feather" />
                    </Button>
                </Left>
                <Body style={{  }}>
                    <Title>Detail Pesanan</Title>
                </Body>
                <Right style={{ flex: 0.15 }} />
            </AppBar>
            { data.detail !== undefined ? 
            <Content>
                <View style={styles.productContainer}>
                    <View style={styles.cardProduct}>
                        <View style={styles.productThumbnail}>
                            <Thumbnail square large source={{uri: data.detail.images[0].src}} style={styles.thumbnail} />
                        </View>
                        <View style={styles.productInfo}>
                            <Text style={styles.productName}>{data.detail.name}</Text>
                            <Text style={styles.productPrice}>{currencyFormat(data.detail.price)}</Text>
                            <Text style={styles.productQty}>Jumlah : {route.params.data.qty}</Text>
                        </View>
                    </View>
                    {/* <View style={styles.productInformation}>
                        <Text style={styles.informationLabel}>Informasi Produk</Text>
                        <HTML html={data.detail.description} baseFontStyle={{fontSize: 16}} imagesMaxWidth={Dimensions.get('window').width} containerStyle={{ width: RFValue(315)}} />
                    </View>
                    <View style={styles.productAdditionalInformation}>
                        <Text style={styles.informationLabel}>Keterangan</Text>
                        <HTML html={data.detail.short_description} baseFontStyle={{fontSize: 16}} imagesMaxWidth={Dimensions.get('window').width} containerStyle={{ width: RFValue(315)}} />
                    </View> */}
                </View>
            </Content>
            :
                <ActivityIndicator 
                    size="large"
                    color={Colors.PRIMARY}
                    style={{ marginTop: 25 }}
                />
            }
            
        </Container>
    )
}

export default DetailProduct;