import React, { useEffect, useState } from "react";
import { View, ImageBackground, Image, TouchableOpacity, TouchableWithoutFeedback, FlatList, Keyboard, Animated } from "react-native";
import { Container, Right, Button, Icon, Badge, Text, Content, Item, Input } from "native-base";
import { getProductCategories, getProducts, showCategories } from '@utils';
import { AppBar, StarRating } from "@components";
import { currencyFormat } from "@constants";
import { useSelector } from "react-redux";
import styles from "./styles";

const Home = ({ navigation }) => {
    const products = useSelector(state => state.productCart)
    const [data, setData] = useState({
        category: [],
        bestSellingProduct: [],
        promo: []   
    })

    const _getProductCategories = () => {
        showCategories()
            .then(res => {
                setData( prevState => ({
                    ...prevState,  
                    category: res
                }));
            })
            .catch(err => {
                console.log('Terjadi Kesalahan',err)
            })
    }

    const _getBestSellingProducts = () => {
        getProducts({ stock_status: 'instock'})
            .then(res => {
                let rest_temp = res
                    .filter((x) => x.total_sales > 0)
                    .slice(0,5)
                    .sort(function (a,b) {
                            a.total_sales - b.total_sales;
                        })
                    .reverse();
                setData(prevState => ({
                    ...prevState,
                    bestSellingProduct: rest_temp
                }))
            })
            .catch(err => {
                console.log(err)
            })

    }

    useEffect(() => {
        _getProductCategories();
        _getBestSellingProducts();
    }, [])

    const pan = React.useRef(new Animated.ValueXY()).current;

    return (
        <Container>
            <AppBar searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Cari produk" showSoftInputOnFocus={false} onFocus={() => {navigation.navigate('searchProducts')}} />
                    </Item>
                <Right style={{flex: 0}}>
                    {products.data.length > 0 ? (
                        <Button transparent style={{ justifyContent: "flex-end" }} onPress={() => navigation.navigate('cart')}>
                            <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                            <Badge style={{ scaleX: 0.8, scaleY: 0.8, position: 'absolute' }} danger>
                                <Text>{products.data.length}</Text>
                            </Badge>
                        </Button>
                    ) : (
                        <Button transparent onPress={() => navigation.navigate('cart')}>
                            <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                        </Button>
                    )}
                </Right>
            </AppBar>
            <Content showsVerticalScrollIndicator={false}>
                <View style={styles.headerContainer}>
                    <ImageBackground source={require("@assets/curved.png")} resizeMode="stretch" style={styles.curvedSegment}>
                        {/* <View style={styles.userInfoSegment}>
                            <Image source={require("@assets/example_profile.jpg")} style={styles.avatar} />
                            <View style={styles.infoSegment}>
                                <Text style={styles.welcome}>Selamat datang</Text>
                                <Text style={styles.userName}>Monkey D. Luffy</Text>
                                <Text style={styles.phoneNumber}>+62 8967 4462 657</Text>
                            </View>
                        </View> */}
                    </ImageBackground>
                </View>


                <View style={styles.categoryContainer}>
                    <View style={styles.cardHeaderSegment}>
                        <View style={styles.labelSegment}>
                            <Text style={styles.label}>Kategori</Text>
                        </View>
                        {/* <View style={styles.viewMoreSegment}>
                            <TouchableOpacity>
                                <Text style={styles.viewMore}>Lihat semua</Text>
                            </TouchableOpacity>
                        </View> */}
                    </View>
                    <View style={styles.listCategory}>
                        <FlatList
                            data={data.category}
                            horizontal
                            keyExtractor={(item, index) => index.toString()}
                            contentContainerStyle={{ paddingVertical: 5 }}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({item, index}) => (
                                <View style={styles.cardButtonContainer}>
                                    <TouchableWithoutFeedback onPress={() => navigation.navigate('orderProduct', {categoryId: item.id, categoryName: item.name})}>
                                        <View style={styles.cardButton}>
                                            <Image source={item.image == null ? require('@assets/icon/plant3x.png') : { uri: item.image.src}} style={styles.categoryIcon} />
                                        </View>
                                    </TouchableWithoutFeedback>
                                    <Text style={styles.categoryTitle}>{item.name}</Text>
                                </View>
                            )}
                        />
                    </View>
                </View>

                {/* <View style={styles.locationAlertBox}>
                    <View style={styles.boxLeft}>
                        <Image source={require("@assets/marker.png")} style={styles.markerIcon} />
                        <View>
                            <Text style={styles.labelLocationRequest}>Cek kurir di daerahmu</Text>
                            <TouchableOpacity>
                                <Text style={styles.btnLocationRequestTitle}>Aktifkan Lokasi</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.boxRight}>
                        <TouchableOpacity>
                            <Icon name="close" type="AntDesign" style={styles.iconCloseBox} />
                        </TouchableOpacity>
                    </View>
                </View> */}

                { data.bestSellingProduct.length > 0 &&
                    <View style={styles.categoryContainer}>
                        <View style={styles.cardHeaderSegment}>
                            <View style={styles.labelSegment}>
                                <Text style={styles.label}>Produk Terlaris</Text>
                            </View>
                            <View style={styles.viewMoreSegment}>
                                <TouchableOpacity>
                                    <Text style={styles.viewMore}>Lihat semua</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={styles.listBestProduct}>
                            <FlatList
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                                data={data.bestSellingProduct}
                                contentContainerStyle={{ paddingTop: 10, paddingStart: 5 }}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => (
                                    <TouchableWithoutFeedback onPress={() => navigation.navigate('orderDetail', { data: item, origin: 'home' })}>
                                        <View style={styles.productCard}>
                                            <Image source={{ uri: item.images[0].src }} style={styles.productThumbnail} />
                                            <View style={styles.productInfo}>
                                                <Text style={styles.productName}>{item.name}</Text>
                                                <Text style={styles.productPrice}>{currencyFormat(item.price)}</Text>
                                                <StarRating ratings={item.average_rating} views={item.rating_count} />
                                            </View>
                                        </View>
                                    </TouchableWithoutFeedback>
                                )}
                            />
                        </View>
                    </View>
                }

                {/* <View style={styles.categoryContainer}>
                    <View style={styles.cardHeaderSegment}>
                        <View style={styles.labelSegment}>
                            <Text style={styles.label}>Promo 2020</Text>
                        </View>
                        <View style={styles.viewMoreSegment}>
                            <TouchableOpacity>
                                <Text style={styles.viewMore}>Lihat semua</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.listBestProduct}>
                        <FlatList
                            horizontal={true}
                            showsHorizontalScrollIndicator={false}
                            data={DATA.products}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({ item, index }) => (
                                <TouchableWithoutFeedback onPress={() => alert(item.name)}>
                                    <View style={styles.productCard}>
                                        <Image source={item.image} style={styles.productThumbnail} />
                                        <View style={styles.productInfo}>
                                            <Text style={styles.productName}>{item.name}</Text>
                                            <Text style={styles.productPrice}>{currencyFormat(item.price)}</Text>
                                            <StarRating ratingObj={item.rating} />
                                        </View>
                                    </View>
                                </TouchableWithoutFeedback>
                            )}
                        />
                    </View>
                </View> */}


            </Content>
        </Container>
    )
}

export default Home;