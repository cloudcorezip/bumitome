import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
import { RFValue } from "react-native-responsive-fontsize";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
    headerContainer: {
        height: width * 0.24,
        backgroundColor: Colors.PRIMARY
    },
    curvedSegment: {
        flex: 1,
        justifyContent: "center"
    },
    userInfoSegment: {
        marginLeft: width * 0.05,
        marginRight: width * 0.05,
        flexDirection: "row",
        alignSelf: "flex-start",
        
    },
    avatar: {
        width: width * 0.18,
        height: width * 0.18,
        borderRadius: 75,
        borderWidth: 4,
        borderColor: Colors.WHITE
    },
    infoSegment: {
        justifyContent: "center",
        marginLeft: width * 0.03
    },
    welcome: {
        color: Colors.WHITE,
        fontSize: Normalize(12),
    },
    userName: {
        fontSize: Normalize(16),
        fontWeight: "bold",
        color: Colors.WHITE
    },
    phoneNumber: {
        color: Colors.WHITE,
        fontSize: Normalize(12),
    },

    // header card labels
    cardHeaderSegment: {
        flexDirection: "row",
        marginLeft: width * 0.02,
        marginRight: width * 0.02,
    },
    labelSegment: {
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "center",
    },
    label: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.BLACK
    },
    viewMoreSegment: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center"
    },
    viewMore: {
        fontSize: Normalize(12),
        fontWeight: "bold",
        color: Colors.PRIMARY
    },

    // category styles
    categoryContainer: {
        marginTop: width * 0.05
    },
    listCategory: {
        margin: width * 0.02,
        marginTop: width * 0.05,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    cardButtonContainer: {
        marginEnd: RFValue(15),
        width: width * 0.2,
        alignItems: "center"
    },
    cardButton: {
        width: width * 0.18,
        height: width * 0.18,
        backgroundColor: Colors.WHITE,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 20,
        elevation: 4,
        marginBottom: width * 0.04
    },
    categoryIcon: {
        width: width * 0.1,
        height: width * 0.1
    },
    categoryTitle: {
        fontSize: Normalize(11),
        color: Colors.BLACK,
        textAlign: "center"
    },
    
    // List best selling products styles
    listBestProduct: {
        margin: width * 0.02,
        marginTop: width * 0.05,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    productCard: {
        width: width * 0.3,
        borderRadius: 10,
        backgroundColor: Colors.WHITE,
        elevation: 4,
        marginRight: width * 0.03,
        marginBottom: width * 0.05
    },
    productThumbnail: {
        width: width * 0.3,
        height: width * 0.25,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10
    },
    productInfo: {
        padding: width * 0.02
    },
    productName: {
        fontSize: Normalize(12),
        fontWeight: "bold",
        color: Colors.BLACK,
    },
    productPrice: {
        fontSize: Normalize(12),
        fontWeight: "bold",
        color: Colors.PRIMARY
    },


    // Location alert box
    locationAlertBox: {
        flex: 1,
        backgroundColor: Colors.GRAY_MEDIUM,
        margin: width * 0.02,
        borderRadius: 10,
        padding: width * 0.02,
        flexDirection: "row"
    },
    boxLeft: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    boxRight: {
        flex: 1,
        alignItems: "flex-end",
    },
    markerIcon: {
        width: width * 0.1,
        height: width * 0.1,
        marginRight: "2%"
    },
    labelLocationRequest: {
        fontSize: Normalize(12),
        color: Colors.BLACK
    },
    btnLocationRequestTitle: {
        fontSize: Normalize(12),
        color: Colors.PRIMARY,
        fontWeight: "bold",
    },
    iconCloseBox: {
        fontSize: Normalize(14),
        color: Colors.BLACK
    }
})