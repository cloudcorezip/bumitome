module.exports = {
    products: [
        {
            id: 1,
            name: "Hidroponik Kailan",
            qty: 10,
            price: 2400,
            rating: {
                ratings: 5,
                views: 520
            },
            image: require('@assets/products/product_1.jpeg')
        },
        {
            id: 2,
            name: "Hidroponik Selada Segar",
            qty: 24,
            price: 3700,
            rating: {
                ratings: 4,
                views: 451
            },
            image: require('@assets/products/product_2.jpeg')
        },
        {
            id: 3,
            name: "Pakcoy Hidroponik",
            qty: 10,
            price: 5500,
            rating: {
                ratings: 5,
                views: 330
            },
            image: require('@assets/products/product_3.jpeg')
        },
        {
            id: 4,
            name: "Kangkung Hidroponik",
            qty: 40,
            price: 2000,
            rating: {
                ratings: 4,
                views: 120
            },
            image: require('@assets/products/product_4.jpeg')
        },
        {
            id: 5,
            name: "Hidroponik Sawi",
            qty: 15,
            price: 3500,
            rating: {
                ratings: 4,
                views: 221
            },
            image: require('@assets/products/product_1.jpeg')
        },
    ]
}