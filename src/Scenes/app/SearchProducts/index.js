import React, { useEffect, useRef, useState } from "react";
import { ActivityIndicator, FlatList, RefreshControl, TouchableOpacity, View } from "react-native";
import { ProductCard } from "@components"
import { calculateTotalPrice } from "@constants";
import { useSelector } from "react-redux";
import { getProducts } from "@utils";
import { Text, Badge, Body, Button, Container, Icon, Input, Item, Left, Right, Title } from "native-base";
import { AppBar } from "@components";
import styles from './styles';


const SearchProducts = ({ route, navigation }) => {
  const productsInStorage = useSelector(state => state.productCart)
  const [data, setData] = useState({
    products: [],
    searchFilter: '',
    searchLoading: false
  })
  const timeoutRef = useRef(null);              

  const _getProducts = (param="?stock_status=instock") => {
    setData(prevState => ({
      ...prevState,
      searchLoading: true
    }));
    getProducts(param)
      .then(res => {
        setData(prevState  =>  ({
          ...prevState,
          products: res.data,
          searchLoading: false
        }))
      })
      .catch(err => {
        console.log(err);
      })
  }

  useEffect(() => {                             
    if (timeoutRef.current !== null) {          
      clearTimeout(timeoutRef.current);         
    }

    timeoutRef.current = setTimeout(()=> {      
      timeoutRef.current = null;                
      data.searchFilter !== '' ? _getProducts("?stock_status=instock&search=" + data.searchFilter) : _getProducts();         
    },500);                                     
    return () => {
      clearTimeout(timeoutRef);
    }
  },[data.searchFilter]);      

  return (
    <Container>
      <AppBar>
          <Left>
              <Button transparent onPress={() => navigation.goBack()}>
                  <Icon name="arrow-left" type="Feather" />
              </Button>
          </Left>
          <Body style={{ alignItems: "center" }}>
            <Title>Cari Semua Produk</Title>
          </Body>
          <Right style={{ flex: 0.45 }}>
            {productsInStorage.data.length > 0 ? (
                    <Button transparent style={{ justifyContent: "flex-end" }} onPress={() => navigation.navigate('cart')}>
                        <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                        <Badge style={{ scaleX: 0.8, scaleY: 0.8, position: 'absolute' }} danger>
                            <Text>{productsInStorage.data.length}</Text>
                        </Badge>
                    </Button>
            ) : (
                    <Button transparent onPress={() => navigation.navigate('cart')}>
                        <Icon name="shopping-cart" type="Feather" style={{ color: "#FFF" }} />
                    </Button>
            )}
          </Right>
      </AppBar>
      <View style={styles.bodyContainer}>
        <View style={styles.searchContainer}>
            <Item regular style={styles.searchItem}>
                <Input
                    style={styles.searchInput}
                    placeholder="Selada . . ."
                    onChangeText={text => setData(prevState => ({
                      ...prevState,
                      searchFilter: text
                    }))}
                    placeholderTextColor="rgba(160, 160, 160, 60)"
                />
                { data.searchLoading &&
                  <ActivityIndicator size={'small'} style={{ marginEnd: 15 }} />
                }
            </Item>
        </View>
        <FlatList
                showsVerticalScrollIndicator={false}
                style={styles.flatListContainer}
                data={data.products}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (
                    <ProductCard
                        name={item.name}
                        thumbnail={item.images[0].src}
                        qty={item.stock_quantity === null ? 0 : item.stock_quantity}
                        price={item.price}
                        onPress={() => navigation.navigate('orderDetail', { data: item, origin: 'searchProducts' })}
                    />
                )}
            />
            {productsInStorage.data.length > 0 ? (
                    <View style={styles.footerContainer}>
                        <View style={styles.footerLeft}>
                            <Text style={styles.labelPrice}>Total Harga</Text>
                            <Text style={styles.valuePrice}>{calculateTotalPrice(productsInStorage)}</Text>
                        </View>
                        <View style={styles.footerRight}>
                            <TouchableOpacity style={styles.button} onPress={() => navigation.navigate('cart', {products: productsInStorage})}>
                                <Icon name="shopping-cart" type="Feather" style={styles.buttonIcon} />
                                <Text style={styles.buttonTitle}>Keranjang</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                ) : (
                    null
                )}
      </View>
    </Container>
  )
}

export default SearchProducts;