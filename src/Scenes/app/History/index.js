import React, { useEffect, useState } from "react";
import { View, Text, TouchableWithoutFeedback, FlatList, RefreshControl } from "react-native";
import { currencyFormat } from "@constants";
import { Container, Body, Title, Icon } from "native-base";
import { Row, Grid } from 'react-native-easy-grid';
import { RFValue } from "react-native-responsive-fontsize";
import moment from "moment";
import { getOrders } from "@utils";
import { AppBar } from "@components";
import styles from "./styles";

const History = ({ navigation }) => {
    const [] = useState('');
    const [data, setData] = useState({
        histories: [],
        loading: true
    });

    const onRefresh = React.useCallback(() => {
        _getOrders();
    })

    const _getOrders = () => {
        getOrders()
            .then(res => {
                setData({
                    histories: res.data.filter(x => x.status === 'completed' ),
                    loading: false
                })
            })
            .catch(err => {
                alert('Terjadi Kesalahan',err.message)
                console.log(err)
            })
    }

    useEffect(() => {
        onRefresh();

    }, [])

    return (
        <Container>
            <AppBar>
                <Body style={{ alignItems: "center" }}>
                    <Title>Riwayat Pesanan</Title>
                </Body>
            </AppBar>
            <View style={styles.bodyContainer}>
                <FlatList 
                    data={data.histories}
                    contentContainerStyle={{ paddingBottom: RFValue(10) }}
                    keyExtractor={(index) => index.toString()}
                    refreshControl={
                        <RefreshControl 
                            refreshing={data.loading}
                            onRefresh={onRefresh}
                        />
                    }
                    renderItem={({ item}) => (
                        <TouchableWithoutFeedback onPress={() => navigation.navigate('listProduct', { data: { orderId: item.id }})}>
                        <Grid style={styles.cardContainer}>
                            <Row style={styles.cardHeader}>
                                <View style={styles.cardLeft}>
                                    <Text style={styles.cardLeftTitle}># {item.number}</Text>
                                    <View style={{flexDirection: "row"}}>
                                        <View style={styles.dateSegment}>
                                            <Icon name="calendar" type="AntDesign" style={styles.dateIcon}/>
                                            <Text style={styles.dateText}>{moment(item.date_paid_gmt).format('DD MMMM YYYY')}</Text>
                                        </View>
                                        <View style={styles.dateSegment}>
                                            <Icon name="clockcircleo" type="AntDesign" style={styles.dateIcon}/>
                                            <Text style={styles.dateText}>{moment(item.date_paid_gmt).format('HH:mm')}</Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={styles.cardRight}>
                                    <Text style={styles.cardRightBody}>{currencyFormat(item.total)}</Text>
                                </View>
                            </Row>
                        </Grid>
                        </TouchableWithoutFeedback>
                    )}

                />
            </View>
        </Container>
    )
}

export default History;