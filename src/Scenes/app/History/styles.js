import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    searchContainer: {
        backgroundColor: '#EBEBEB',
        padding: width * 0.024,
    },
    searchItem: {
        backgroundColor: "#D7DBDB",
        borderRadius: 75,
        height: 40  
    },
    searchInput: {
        paddingLeft: width * 0.03
    },
    bodyContainer: {
        flex: 1
    },
    cardContainer: {
        backgroundColor: '#FFFFFF',
        marginLeft: width * 0.03,
        marginRight: width * 0.03,
        marginTop: width * 0.03,
        borderRadius: 5,
        elevation: 4
    },
    cardHeader: {
        borderBottomWidth: 0.5,
        borderBottomColor: "rgba(112, 112, 112, 0.4)",
        flexDirection: "row",
        padding: width * 0.04
    },
    cardLeft: {
        flex: 1,
        flexDirection: "column"
    },
    cardLeftTitle: {
        fontSize: Normalize(16),
        color: Colors.BLACK,
        fontWeight: "bold"
    },
    dateSegment: {
        flexDirection: "row",
        alignItems: "center",
        marginRight: width * 0.02,
        marginTop: width * 0.015
    },
    dateIcon: {
        fontSize: Normalize(13),
        marginRight: width * 0.01,
        color: Colors.BLACK_LIGHT
    },
    dateText: {
        fontSize: Normalize(12),
        color: Colors.BLACK_LIGHT
    },
    cardRight: {
        flex: 1,
        flexDirection: "column",
        alignItems: "flex-end",
        justifyContent: "center"
    },
    cardRightBody: {
        fontSize: Normalize(14),
        fontWeight: "bold",
        color: Colors.ORANGE
    }
})