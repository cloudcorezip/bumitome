import React, { useState, useContext, useEffect } from "react";
import { View, TouchableOpacity, Image, ImageBackground, ActivityIndicator, Alert } from "react-native";
import { AuthContext } from "@constants/context";
import { AppBar } from "@components";
import { signIn, getUserProfile } from "@utils";
import { Container, Text, Right, Button, Icon, Form, Item, Input, Label, Content } from "native-base";
import styles from "./styles";
import { RFValue } from "react-native-responsive-fontsize";
import { storeUserProfile } from "@actions";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from "react-redux";


const ProfileBody = (props) => {
    if(props.token) {
        return (
            <View style={{ marginHorizontal: RFValue(20), marginTop: RFValue(60)}}>
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text>{props.data.first_name} {props.data.last_name}</Text>
                    <Text style={{ marginTop: RFValue(15)}}>{props.data.email}</Text>
                </View>
                <View style={{ marginTop: RFValue(35 )}}>
                    <TouchableOpacity>
                        <View style={{ borderBottomWidth: 1, flexDirection: 'row', paddingBottom: 10, borderColor: '#DDDDDD' }}>
                            <View style={{ flex: 1}}>
                                <Text>Profile</Text>
                            </View>
                            <View style={{  justifyContent: 'center' }}>
                                <Icon type="AntDesign" name="right" style={{ fontSize: RFValue(10) }}/>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={{ marginVertical: RFValue(10) }} />
                    <TouchableOpacity>
                        <View style={{ borderBottomWidth: 1, flexDirection: 'row', paddingBottom: 10, borderColor: '#DDDDDD' }}>
                            <View style={{ flex: 1}}>
                                <Text>Data Alamat</Text>
                            </View>
                            <View style={{  justifyContent: 'center' }}>
                                <Icon type="AntDesign" name="right" style={{ fontSize: RFValue(10) }}/>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    } else {
        return (
            <Form style={styles.formSegement}>
            <Item stackedLabel style={styles.itemInput}>
                <Label>Email</Label>
                <Input onChangeText={(text) => props.onChangeEmail(text)} />
            </Item>
            <Item stackedLabel style={styles.itemInput}>
                <Label>Password</Label>
                <Input onChangeText={(text) => props.onChangePassword(text)}/>
            </Item>
        </Form>

        )
    }
}


const Profile = ({ navigation }) => {
    const [state, setState] = useState({})
    const [loggedIn, isLoggedIn ] = useState(false)
    const [loading, isLoading] = useState(false)
    const [screenLoading, setScreenLoading] = useState(true);

    const { signOut } = useContext(AuthContext)

    const userData = useSelector(state => state.userData.data)
    const dispatch = useDispatch();


    useEffect(() => {
        checkToken();
    }, [loggedIn]);


    const checkToken = async () => {
        let token = await AsyncStorage.getItem('token');
        if(token) {
            isLoggedIn(true)
        } else {
            isLoggedIn(false)
        }
    }


    const handlePress = () => {
        isLoading(true)
        signIn({
            username: state.email ? state.email : '',
            password: state.password ? state.password : '' 
        }).then( async res => {
            try {
                await AsyncStorage.setItem('token', res.data.token);
                getProfile(res.data.user_id)
            } catch (err) {
                console.log(err)
            }
        }).catch(err => {
            if(err.response.status == 403) {
                alert('Email/Password Kurang Tepat')
            } else if(err.response.status == 401) {
                dispatch(storeUserProfile(undefined))
                isLoading(false)
            } else {
                alert('Terjadi Kesalahan')
            }
        })
    }

    const getProfile = async (userId) => {
        getUserProfile(userId).then(res => {
            dispatch(storeUserProfile(res));
            alert('Login Berhasil')
            isLoggedIn(true);
            isLoading(false)
            navigation.navigate('home')
        }).catch(err => {
            console.log(err)
        })
    }

    const updateInput = (stateName, text)  => {
        setState(prevState => ({
            ...prevState,
            [stateName]: text
        }))
    }

    const logoutModal = () => {
        if(loggedIn) {
            Alert.alert(
                'Log out',
                'Anda yakin ingin Log out',
                [
                    {
                        text: "Cancel",
                        onPress: () => console.log('canceled'),
                        style: "cancel"
                    },
                    { text: "Log Out", onPress: () => handleLogout()}
                ]
            )
        } else {
            navigation.navigate('signUp')
        }
    }

    const handleLogout = async () => {
        AsyncStorage.removeItem('token')
        // dispatch(storeUserProfile(undefined));
        isLoggedIn(false)
        // navigation.navigate('home');
    }
    
    return (
        <Container>
            <AppBar >
                <Right>
                    {/* <Button transparent onPress={() => signOut()}>
                        <Icon name="logout" type="AntDesign" />
                    </Button> */}
                    <TouchableOpacity style={{  marginRight: 15 }} onPress={() => logoutModal()}>
                        <Text style={{ color: 'white' }}>{!loggedIn ? 'Daftar' : 'Log Out'}</Text>
                    </TouchableOpacity>
                </Right>                
            </AppBar>
            <Content style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
                <View style={styles.headerContainer}>
                    <Image source={require("@assets/curved.png")} style={styles.headerCurved} />
                </View>
                { loggedIn &&
                    <ImageBackground source={{ uri: userData.avatar_url }} imageStyle={styles.avatarStyle} style={styles.avatar}>
                    </ImageBackground>
                }
                <View style={styles.bodyContainer}>
                    <ProfileBody 
                        onChangeEmail={(text) => updateInput('email', text)} 
                        onChangePassword={(text) => updateInput('password', text)} 
                        token={loggedIn} 
                        data={userData}
                    />
                    { !loggedIn && 
                        <TouchableOpacity disabled={loading} style={styles.saveButton} onPress={() => handlePress()}>
                            { loading ? 
                                <ActivityIndicator  color="#ffff" />
                                :
                                <Text style={styles.saveButtonTitle}>Masuk</Text>
                            }
                        </TouchableOpacity>
                    }
                </View>
            </Content>
        </Container>
    )
}

export default Profile;