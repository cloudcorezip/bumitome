import { StyleSheet, Dimensions } from "react-native"
import { Colors, Normalize } from "@styles";
import { RFValue } from "react-native-responsive-fontsize";
const { width } = Dimensions.get("window");

export default StyleSheet.create({
    headerContainer: {
        flex: 0.2,
        backgroundColor: Colors.PRIMARY,
        justifyContent: "flex-end",
        zIndex: 0
    },
    headerCurved: {
        width: width,
        height: width * 0.21
    },

    bodyContainer: {
        flex: 1,
        zIndex: 0,
    },
    avatarStyle: {
        borderRadius: 75, 
        borderWidth: 5, 
        borderColor: Colors.WHITE
    },
    avatar: {
        width: width * 0.25,
        height: width * 0.25,
        position: "absolute",
        zIndex: 1,
        alignSelf: "center",
        marginTop: width * 0.085,
        alignItems: "flex-end",
        justifyContent: "flex-end",
        borderRadius: 75,
        elevation: 8
    },
    buttonCamera: {
        width: width * 0.1,
        height: width * 0.1,
        backgroundColor: Colors.PRIMARY,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 75,
        borderWidth: 3,
        borderColor: Colors.WHITE,
    },
    iconCamera: {
        fontSize: Normalize(16),
        color: Colors.WHITE
    },

    formSegement: {
        marginTop: RFValue(50),
    },
    itemInput: {
        marginBottom: width * 0.015,
        marginHorizontal: RFValue(20),
        marginLeft: RFValue(20)
    },

    saveButton: {
        marginTop: RFValue(70), 
        width: width * 0.9,
        height: width * 0.12,
        alignSelf: "center",
        backgroundColor: Colors.PRIMARY,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5,
    },
    saveButtonTitle: {
        fontSize: Normalize(16),
        color: Colors.WHITE
    }
})