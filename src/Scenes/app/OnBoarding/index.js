import React from "react";
import { View, Text, Image, StatusBar } from "react-native";
import { Container } from "native-base";
import { Button } from "@components";

import styles from "./styles";
import AsyncStorage from '@react-native-async-storage/async-storage';

const OnBoarding = ({ navigation }) => {
    const prepareApp = async () => {
        await AsyncStorage.setItem("firstInstall", JSON.stringify(false))
        navigation.navigate("signIn")
    }
    return (
        <Container style={styles.container}>
            <StatusBar barStyle="dark-content" />
            <View style={styles.headerContainer}>
                <Image source={require("@assets/social.png")} style={styles.headerImage}/>
                <Text style={styles.headerTitle}>Welcome!</Text>
                <Text style={styles.headerSlug}>Sayuran hidroponik pertama{"\n"}Di Bandar Lampung</Text>
            </View>
            <View style={styles.bodyContainer}>
                <Text style={styles.bodyDescription}>Sayuran ini asli pemberdayaan dari Kebun kami yang fresh. Maka itu untuk stock yang tersedia bisa berubah setiap harinya karena menyesuaikan panen</Text>
                <Text style={styles.bodyClosing}>Salam hangat{"\n"}Petani Bumi To Me</Text>
                <Button 
                    onPress={() => prepareApp()}
                    title="Get Started" 
                    width={0.6} on/>
            </View>
        </Container>
    )
}

export default OnBoarding;