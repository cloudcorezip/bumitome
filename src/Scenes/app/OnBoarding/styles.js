import { StyleSheet, Dimensions } from "react-native";
import { Colors, Normalize } from "@styles";
const { width, height } = Dimensions.get("window");

export default StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    headerContainer: {
        flex: 1.2,
        width: "100%",
        alignItems: "center",
        justifyContent: "center",
    },
    headerImage: {
        width: width * 0.55,
        height: width * 0.41,
        marginTop: width * 0.05
    },
    headerTitle: {
        fontSize: Normalize(20),
        fontWeight: "bold",
        color: Colors.BLACK,
        alignSelf: "center",
        marginTop: width * 0.05
    },
    headerSlug: {
        fontSize: Normalize(14),
        color: Colors.BLACK,
        fontWeight: "bold",
        textAlign: 'center',
        marginTop: width * 0.07
    },

    bodyContainer: {
        flex: 1
    },
    bodyDescription: {
        fontSize: Normalize(14),
        color: Colors.BLACK,
        paddingBottom: width * 0.05,
        paddingLeft: width * 0.1,
        paddingRight: width * 0.1,
        textAlign: "center",
    },
    bodyClosing: {
        fontSize: Normalize(16),
        color: Colors.BLACK,
        textAlign: "center",
        paddingBottom: width * 0.2
    }
})