// auth
import SignIn from "./auth/SignIn";
import SignUp from "./auth/SignUp";
import OnBoarding from "./app/OnBoarding";
import ResetPassword from "./auth/ResetPassword";
// import Register from "./auth/Register";

import Home from "./app/Home";

import OrderList from "./app/OrderList";
import ListProduct from "./app/OrderList/ListProduct";
import DetailProduct from "./app/OrderList/DetailProduct";

import Category from "./app/Order";
import OrderProduct from "./app/Order/ListProducts";
import OrderDetail from "./app/Order/OrderDetail";
import Cart from "./app/Order/Cart";
import OrderSuccess from "./app/Order/OrderSuccess";

import History from "./app/History";

import Profile from "./app/Profile";
import SearchProducts from "./app/SearchProducts"


export { 
    SignIn, 
    SignUp, 
    OnBoarding, 
    ResetPassword,
    // Register,
    Home, 
    OrderList,
    OrderProduct,
    History,
    Profile,
    ListProduct,
    DetailProduct,
    OrderDetail,
    Cart,
    OrderSuccess,
    Category,
    SearchProducts,
}