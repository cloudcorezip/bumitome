/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React from 'react';
import { AuthContext } from "@constants/context";
import RootNavigator from "@navigations";
import SplashScreen from "react-native-splash-screen";
import { Provider } from "react-redux";
import configureStore from "@stores/configureStore";
import { PersistGate } from "redux-persist/integration/react";
console.disableYellowBox = true;


const App = () => {
  const [ userToken, setUserToken ] = React.useState(null);
  const { persistor, store } = configureStore()

  React.useEffect(() => {
    SplashScreen.hide()
  }, [])
  
  const authContext = React.useMemo(() => {
    return {
      signIn: async (username, password) => {
        setUserToken('dummy-token')
      },
      signOut: () => {
        setUserToken(null)
      }
    }
  })

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <AuthContext.Provider value={authContext}>
          <RootNavigator userToken={userToken}/>
        </AuthContext.Provider>
      </PersistGate>
    </Provider>
  );
};

export default App;
