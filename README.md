# BUMI TO ME v0.1

- BUMI TO ME merupakan aplikasi E-Commerce atau pemesanan tanaman hidroponik yang pertama di bandar lampung!
- Tanaman yang dijual asli dari kebun yang ada di bandar lampung! dan stock yang tersedia bisa berubah setiap harinya karena menyesuaikan panen

## Main Feature
- [ ] Login
- [ ] Register
- [ ] Reset Password
- [ ] Dashboard
- [ ] Pemesanan
    - List Pemesanan
    - Daftar List Pemesanan
    - Detail Pemesanan
- [ ] Pesan / Order
    - List Produk
    - Detail Produk
- [ ] Keranjang
    - List Produk
    - Checkout
    - Order Success (Jika pesanan sudah dilakukan)
- [ ] Riwayat
    - List Riwayat
    - Detail Riwayat
    - Detail Riwayat per produk
- [ ] Profile

## Authentication and Authorization 
- Pembeli
Pembeli merupakan orang-orang yang akan memesan produk tanaman hidroponik melalui aplikasi
    - Registrasi
        - Nama Lengkap
        - Alamat Email
        - Password / Kata Sandi
    - Login 
        - Alamat Email
        - Password / Kata Sandi
    - Feature
        - Membuat Pesanan
        - Melihat info tentang produk
        - Menentukan alamat pengiriman

## Business Logic
- ?

### Step-step pemesanan
1. Pembeli melakukan login atau registrasi terlebih dahulu
2. Memilih produk tanaman hidroponik
3. Memasukkan kedalam keranjang
4. Melakukan checkout
5. Melengkapi Nama Penerima, Alamat dan Email
6. Melakukan pesanan

#### Lain-lain
- ?

#### Mobile Mockup
https://xd.adobe.com/view/a4c4ecce-47fd-45f4-6888-47534d327efc-d2f5/?fullscreen ( MVP: users )